# CoBAT

CoBAT(Conversation-Based AI Assistant Teaching Tool) a tool for designing and teaching AI assistants

**Note:** You can install and use the production version [here](https://gitlab.com/phamnam.mta/cobat-production)

## Development

### Installation

1. CoBAT is a Meteor app, so the first step is to [install Meteor](https://www.meteor.com/install)

2. Then clone this repo and install the dependencies:

```bash
git clone https://gitlab.ftech.ai/nlp/research/cobat.git
cd cobat/cobat
meteor npm install
```

3. CoBAT needs to be connected to other services, especially Rasa. To do this, you need to install and run Rasa server follow the instructions [here](https://gitlab.com/phamnam.mta/rasa-for-cobat)

4. Make sure you are in the project root(`cobat/cobat`) and run cobat with `meteor run`. CoBAT will be available at http://localhost:3000 so open your browser and happy editing.

