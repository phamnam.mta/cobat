/* eslint-disable import/order */
/* eslint-disable prefer-const */
import React from 'react';

let canExport = (role, scope = Roles.GLOBAL_GROUP, userId = Meteor.userId()) => {
    if (Roles.userIsInRole(userId, [role, 'global-admin'], scope)) {
        return true;
    }
    return false;
};
let checkIfCanExport = (role, scope = Roles.GLOBAL_GROUP, userId = Meteor.userId()) => {
    if (Roles.userIsInRole(userId, [role, 'global-admin'], scope)) {
        return;
    }
    throw new Meteor.Error('unauthorized', "Not authorized to execute this function");
};
let getUserScopesExport = () => {};
let checkIfScopeExport = () => {};
let getScopesForUserExport = () => {};
let isUserPermissionGlobalExport = () => {};
let areScopeReadyExport = () => true;
let setScopesExport = () => {};
let CanExport = props => (
    <>
        {props.children}
    </>
);

export const getUserScopes = getUserScopesExport;
export const checkIfScope = checkIfScopeExport;
export const isUserPermissionGlobal = isUserPermissionGlobalExport;
export const getScopesForUser = getScopesForUserExport;
export const can = canExport;
export const checkIfCan = checkIfCanExport;
export const areScopeReady = areScopeReadyExport;
export const setScopes = setScopesExport;
export const Can = CanExport;
