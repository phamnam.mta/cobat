import React from "react";
import PropTypes from "prop-types";
import Alert from "react-s-alert";
import moment from "moment";
import { Meteor } from "meteor/meteor";
import { withTracker } from "meteor/react-meteor-data";
import { connect } from "react-redux";
import {
  Button,
  Message,
  Tab,
  Checkbox,
  Table,
  Popup,
  Modal,
  Confirm,
} from "semantic-ui-react";
import { isEmpty } from "lodash";
import { saveAs } from "file-saver";

import IntentReport from "../evaluation/IntentReport";
import EntityReport from "../evaluation/EntityReport";
import IconButton from "../../common/IconButton";
import CompareModels from "./CompareModels";
import { Evaluations } from "../../../../api/nlu_evaluation";
import { Loading } from "../../utils/Utils";

import "react-select/dist/react-select.css";

class ModelsTable extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      data: [],
      selectedModel: [],
      confirmOpen: false,
      openPopupCompare: false,
      downloading: false,
      popupDetailReport: {
        open: false,
        evaluation: {},
      },
    };
  }

  componentWillReceiveProps(props) {
    const { model: { versions: versions } = {}, evaluations } = props;

    const data = [];

    if (!!versions && versions.length > 0) {
      for (let i = 0; i < versions.length; i++) {
        data.push({
          ...evaluations.find((e) => e.modelVersion === versions[i]._id),
          ...versions[i],
        });
      }
    }

    this.setState({ data: data });
  }

  handleOnChange = (checked, id) => {
    const { selectedModel } = this.state;
    if (checked) {
      this.setState({
        selectedModel: [...selectedModel, id],
      });
    } else {
      const index = selectedModel.indexOf(id);
      if (index > -1) {
        selectedModel.splice(index, 1);
        this.setState({
          selectedModel: [...selectedModel],
        });
      }
    }
  };

  removeModels = (ids) => {
    const {
      projectId,
      model: { _id: modelId },
    } = this.props;
    Meteor.call("rasa.deleteNlu", ids, projectId, modelId, (err, res) => {
      if (err) {
        Alert.error(`Error: ${err.reason || err.error || err.message}`, {
          position: "top-right",
          timeout: 2000,
        });
        return;
      }
      this.setState({ confirmOpen: false });
    });
  };

  downLoadModel = (id) => {
    const {
      projectId,
      model: { _id: modelId },
    } = this.props;

    Meteor.call("rasa.download", id, projectId, modelId, (err, res) => {
      if (err) {
        Alert.error(
          `Error: ${err.reason.message || err.error || err.message}`,
          {
            position: "top-right",
            timeout: 5000,
          }
        );
        return;
      }

      const { data, filename, contentType } = res;

      const blob = new Blob([data], { type: contentType });
      saveAs(blob, filename);
    });
  };

  handleItemClick = (id) => {
    const evaluation = this.state.data.find((d) => d._id === id);
    if (!!evaluation) {
      this.setState({
        popupDetailReport: {
          open: true,
          evaluation: evaluation,
        },
      });
    }
  };

  activeModel = (id) => {
    const {
      projectId,
      model: { _id: modelId },
    } = this.props;

    Meteor.call("rasa.activeModel", id, projectId, modelId, (err, res) => {
      if (err) {
        Alert.error(
          `Error: ${err.reason.message || err.error || err.message}`,
          {
            position: "top-right",
            timeout: 5000,
          }
        );
        return;
      }
    });
  };

  getPrimaryPanes() {
    const {
      evaluation: {
        results: {
          intent_evaluation: intentEvaluation,
          entity_evaluation: entityEvaluation,
        } = {},
      } = {},
    } = this.state.popupDetailReport;

    const menuItems = [];
    if (intentEvaluation) {
      menuItems.push({
        menuItem: "Intents",
        render: () => <IntentReport {...intentEvaluation} />,
      });
    }
    if (
      entityEvaluation &&
      Object.keys(entityEvaluation).length > 0 &&
      !isEmpty(entityEvaluation["report"])
    ) {
      menuItems.push({
        menuItem: "Entities",
        render: () => <EntityReport {...entityEvaluation} />,
      });
    }

    return menuItems;
  }

  renderDetailReport(open) {
    const menuItems = this.getPrimaryPanes();
    return (
      <Modal
        content={
          open && (
            <Tab.Pane>
              <Tab
                menu={{ pointing: true, secondary: true }}
                panes={menuItems}
              />
            </Tab.Pane>
          )
        }
        open={open && menuItems.length > 0}
        onClose={() =>
          this.setState({
            popupDetailReport: {
              open: false,
              evaluation: {},
            },
          })
        }
      />
    );
  }

  renderCompareContent() {
    const { selectedModel, data } = this.state;

    const evaluations = [];
    for (let i = 0; i < selectedModel.length; i++) {
      const { results = {}, name } =
        data.find((e) => e.modelVersion === selectedModel[i]) || {};
      evaluations.push({
        results: {
          ...results,
        },
        modelName: name,
      });
    }

    const menuItems = [];
    if (evaluations.length >= 2) {
      const {
        results: {
          intent_evaluation: intentEvaluation1,
          entity_evaluation: entityEvaluation1,
        } = {},
        modelName: modelName1 = "",
      } = evaluations[0];

      const {
        results: {
          intent_evaluation: intentEvaluation2,
          entity_evaluation: entityEvaluation2,
        } = {},
        modelName: modelName2 = "",
      } = evaluations[1];

      if (
        intentEvaluation1 &&
        intentEvaluation2 &&
        !isEmpty(intentEvaluation1["report"]) &&
        !isEmpty(intentEvaluation2["report"])
      ) {
        menuItems.push({
          menuItem: "Intents",
          render: () => (
            <CompareModels
              report1={intentEvaluation1["report"]}
              report2={intentEvaluation2["report"]}
              model1={modelName1}
              model2={modelName2}
              labelType="intent"
            />
          ),
        });
      }
      if (
        entityEvaluation1 &&
        entityEvaluation2 &&
        !isEmpty(entityEvaluation1["report"]) &&
        !isEmpty(entityEvaluation2["report"])
      ) {
        menuItems.push({
          menuItem: "Entities",
          render: () => (
            <CompareModels
              report1={entityEvaluation1["report"]}
              report2={entityEvaluation2["report"]}
              model1={modelName1}
              model2={modelName2}
              labelType="entity"
            />
          ),
        });
      }
    }

    return menuItems.length > 0 ? (
      <Tab menu={{ pointing: true, secondary: true }} panes={menuItems} />
    ) : (
      <Tab.Pane>
        <Message
          success
          header="The model has not been evaluated"
          content="Evaluate the model before comparing."
        />
      </Tab.Pane>
    );
  }

  renderTableRow(rowData) {
    const {
      results: {
        intent_evaluation: intentEvaluation,
        entity_evaluation: entityEvaluation,
      } = {},
    } = rowData;

    const existIntentEva =
      intentEvaluation && !isEmpty(intentEvaluation["report"]);
    const existEntityEva =
      entityEvaluation && !isEmpty(entityEvaluation["report"]);

    const scoreView = (value) => (
      <span
      // style={{
      //   backgroundColor: `rgba(0, 128, 0, ${value})`,
      // }}
      >
        {(Math.round(value * 100) / 100).toString()}
      </span>
    );

    return (
      <Table.Row
        key={rowData._id}
        onClick={(e) => this.handleItemClick(rowData._id)}
      >
        <Table.Cell
          collapsing
          onClick={(e) => {
            e.preventDefault();
            e.stopPropagation();
          }}
        >
          <Checkbox
            style={{ cursor: "pointer" }}
            slider
            onChange={(e, { checked }) => {
              e.preventDefault();
              e.stopPropagation();
              this.handleOnChange(checked, rowData._id);
            }}
          />
        </Table.Cell>

        <Table.Cell
          onClick={(e) => {
            e.preventDefault();
            e.stopPropagation();
          }}
        >
          <Button
            style={{ textDecoration: "underline" }}
            onClick={(e) => {
              e.preventDefault();
              e.stopPropagation();
              this.downLoadModel(rowData._id);
            }}
          >
            {rowData.name}
          </Button>
        </Table.Cell>
        <Table.Cell style={{ cursor: "pointer" }}>
          {moment(rowData.createdAt).format("lll")}
        </Table.Cell>
        <Table.Cell style={{ cursor: "pointer" }}>
          {existIntentEva ? (
            <>{scoreView(intentEvaluation.f1_score)}</>
          ) : (
            <>-</>
          )}
        </Table.Cell>
        <Table.Cell style={{ cursor: "pointer" }}>
          {existIntentEva ? (
            <>{scoreView(intentEvaluation.precision)}</>
          ) : (
            <>-</>
          )}
        </Table.Cell>
        <Table.Cell style={{ cursor: "pointer" }}>
          {existIntentEva ? (
            <>{scoreView(intentEvaluation.accuracy)}</>
          ) : (
            <>-</>
          )}
        </Table.Cell>
        <Table.Cell style={{ cursor: "pointer" }}>
          {existEntityEva ? (
            <>{scoreView(entityEvaluation.f1_score)}</>
          ) : (
            <>-</>
          )}
        </Table.Cell>
        <Table.Cell style={{ cursor: "pointer" }}>
          {existEntityEva ? (
            <>{scoreView(entityEvaluation.precision)}</>
          ) : (
            <>-</>
          )}
        </Table.Cell>
        <Table.Cell style={{ cursor: "pointer" }}>
          {existEntityEva ? (
            <>{scoreView(entityEvaluation.accuracy)}</>
          ) : (
            <>-</>
          )}
        </Table.Cell>
        <Table.Cell collapsing>
          <Popup
            position="top center"
            disabled={rowData.status}
            trigger={
              <div>
                <IconButton
                  icon="check"
                  size="small"
                  active={rowData.status}
                  color={rowData.status ? "green" : "grey"}
                  onClick={(e) => {
                    e.preventDefault();
                    e.stopPropagation();
                    this.activeModel(rowData._id);
                  }}
                />
              </div>
            }
            inverted
            content="Activate this model"
          />
        </Table.Cell>
      </Table.Row>
    );
  }

  render() {
    const { loading } = this.props;
    const {
      selectedModel,
      confirmOpen,
      openPopupCompare,
      popupDetailReport: { open: openPopupDetail },
    } = this.state;

    const { data } = this.state;

    return (
      <Tab.Pane textAlign="center">
        <Loading loading={loading}>
          {data.length > 0 ? (
            <Table celled selectable structured textAlign="center">
              <Table.Header>
                <Table.Row>
                  <Table.HeaderCell rowSpan="2" />
                  <Table.HeaderCell rowSpan="2">Model name</Table.HeaderCell>
                  <Table.HeaderCell rowSpan="2">Created at</Table.HeaderCell>
                  <Table.HeaderCell colSpan="3">Intents</Table.HeaderCell>
                  <Table.HeaderCell colSpan="3">Entities</Table.HeaderCell>
                  <Table.HeaderCell rowSpan="2">States</Table.HeaderCell>
                </Table.Row>
                <Table.Row>
                  <Table.HeaderCell>F1-score</Table.HeaderCell>
                  <Table.HeaderCell>Precision</Table.HeaderCell>
                  <Table.HeaderCell>Accuracy</Table.HeaderCell>

                  <Table.HeaderCell>F1-score</Table.HeaderCell>
                  <Table.HeaderCell>Precision</Table.HeaderCell>
                  <Table.HeaderCell>Accuracy</Table.HeaderCell>
                </Table.Row>
              </Table.Header>

              <Table.Body>
                {data
                  .sort((a, b) => b.createdAt - a.createdAt)
                  .map((d) => this.renderTableRow(d))}
              </Table.Body>

              <Table.Footer fullWidth>
                <Table.Row>
                  <Table.HeaderCell />
                  <Table.HeaderCell colSpan="9">
                    <Modal
                      style={{ width: "auto" }}
                      content={
                        openPopupCompare ? this.renderCompareContent() : ""
                      }
                      open={openPopupCompare}
                      onOpen={() => this.setState({ openPopupCompare: true })}
                      onClose={() => this.setState({ openPopupCompare: false })}
                      trigger={
                        <Button
                          floated="left"
                          icon="law"
                          labelPosition="left"
                          color="blue"
                          size="small"
                          content="Compare"
                          disabled={selectedModel.length !== 2}
                        />
                      }
                    />

                    <Button
                      floated="right"
                      size="small"
                      icon="trash"
                      labelPosition="left"
                      color="red"
                      content="Delete"
                      disabled={selectedModel.length <= 0}
                      onClick={() => this.setState({ confirmOpen: true })}
                    />
                    <Confirm
                      open={confirmOpen}
                      header={`Delete ${
                        selectedModel.length > 1 ? selectedModel.length : ""
                      } ${selectedModel.length > 1 ? "models" : "model"}`}
                      content="This cannot be undone!"
                      onCancel={() => this.setState({ confirmOpen: false })}
                      onConfirm={() => this.removeModels(selectedModel)}
                    />
                  </Table.HeaderCell>
                </Table.Row>
              </Table.Footer>
            </Table>
          ) : (
            <Message
              success
              header="No versions"
              content="Please train and evaluate the model."
            />
          )}
          {openPopupDetail && this.renderDetailReport(openPopupDetail)}
        </Loading>
      </Tab.Pane>
    );
  }
}

ModelsTable.propTypes = {
  loading: PropTypes.bool.isRequired,
  model: PropTypes.object.isRequired,
  evaluations: PropTypes.array.isRequired,
  projectId: PropTypes.string.isRequired,
};

ModelsTable.defaultProps = { model: {}, evaluations: [] };

const ModelsTableContainer = withTracker((props) => {
  const { model } = props;

  // setup model subscription
  const modelHandler = Meteor.subscribe("nlu_models", model._id);
  const evalsHandler = Meteor.subscribe("nlu_evaluations", model._id);
  const loading = !evalsHandler.ready() && !modelHandler.ready();

  const evaluations = Evaluations.find({ modelId: model._id }).fetch();
  return { model, evaluations, loading };
})(ModelsTable);

const mapStateToProps = (state) => ({
  projectId: state.settings.get("projectId"),
});

export default connect(mapStateToProps)(ModelsTableContainer);
