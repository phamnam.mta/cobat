import PropTypes from "prop-types";
import React from "react";
import { Segment, Table, Message } from "semantic-ui-react";
import { getReportData } from '../../../../lib/utils'

export default class CompareModels extends React.Component {
  constructor(props) {
    super(props);

    const { labelType } = this.props;

    this.state = { labelType: labelType };
  }

  renderTableRow(data, index) {
    const scoreView = (value, highlight) => (
      <span
        style={
          highlight
            ? {
                backgroundColor: `rgba(0, 128, 0, ${value})`,
              }
            : {}
        }
      >
        {Math.round(value * 100) / 100}
      </span>
    );

    const evalReport = Object.values(data);

    return (
      <Table.Row key={index}>
        <Table.Cell>{evalReport[0][this.state.labelType]}</Table.Cell>
        <Table.Cell>
          {scoreView(
            evalReport[0]['f1-score'],
            evalReport[0]['f1-score'] > evalReport[1]['f1-score']
          )}
        </Table.Cell>
        <Table.Cell>
          {scoreView(
            evalReport[1]['f1-score'],
            evalReport[0]['f1-score'] < evalReport[1]['f1-score']
          )}
        </Table.Cell>

        <Table.Cell>
          {scoreView(
            evalReport[0].precision,
            evalReport[0].precision >
              evalReport[1].precision
          )}
        </Table.Cell>
        <Table.Cell>
          {scoreView(
            evalReport[1].precision,
            evalReport[0].precision <
              evalReport[1].precision
          )}
        </Table.Cell>

        <Table.Cell>
          {scoreView(
            evalReport[0].recall,
            evalReport[0].recall > evalReport[1].recall
          )}
        </Table.Cell>
        <Table.Cell>
          {scoreView(
            evalReport[1].recall,
            evalReport[0].recall < evalReport[1].recall
          )}
        </Table.Cell>

        <Table.Cell>
          {scoreView(
            evalReport[0].support,
            evalReport[0].support > evalReport[1].support
          )}
        </Table.Cell>
        <Table.Cell>
          {scoreView(
            evalReport[1].support,
            evalReport[0].support < evalReport[1].support
          )}
        </Table.Cell>
      </Table.Row>
    );
  }

  render() {
    const { report1, report2, model1, model2, labelType } = this.props;

    const modelName1 = model1.split("-");
    const modelName2 = model2.split("-");

    const data1 = getReportData(report1, this.state.labelType);
    const data2 = getReportData(report2, this.state.labelType);
    const data = [];

    if (data1.length > 0 && data2.length > 0) {
      for (let i = 0; i < data1.length; i++) {
        const found = data2.find(
          (e) => e[this.state.labelType] === data1[i][this.state.labelType]
        );
        if (!!found) {
          let obj = {};
          obj[`model1-${i}`] = data1[i];
          obj[`model2-${i}`] = found;
          data.push(obj);
        }
      }
    }

    return data.length > 0 ? (
      <Segment>
        <Table celled structured striped textAlign="center">
          <Table.Header>
            <Table.Row>
              <Table.HeaderCell rowSpan="2">
                {labelType.charAt(0).toUpperCase() + labelType.slice(1)}
              </Table.HeaderCell>
              <Table.HeaderCell colSpan="2">F1-score</Table.HeaderCell>
              <Table.HeaderCell colSpan="2">Precision</Table.HeaderCell>
              <Table.HeaderCell colSpan="2">Recall</Table.HeaderCell>
              <Table.HeaderCell colSpan="2">Support</Table.HeaderCell>
            </Table.Row>
            <Table.Row>
              <Table.HeaderCell>
                {modelName1[modelName1.length - 1]}
              </Table.HeaderCell>
              <Table.HeaderCell>
                {modelName2[modelName2.length - 1]}
              </Table.HeaderCell>

              <Table.HeaderCell>
                {modelName1[modelName1.length - 1]}
              </Table.HeaderCell>
              <Table.HeaderCell>
                {modelName2[modelName2.length - 1]}
              </Table.HeaderCell>

              <Table.HeaderCell>
                {modelName1[modelName1.length - 1]}
              </Table.HeaderCell>
              <Table.HeaderCell>
                {modelName2[modelName2.length - 1]}
              </Table.HeaderCell>

              <Table.HeaderCell>
                {modelName1[modelName1.length - 1]}
              </Table.HeaderCell>
              <Table.HeaderCell>
                {modelName2[modelName2.length - 1]}
              </Table.HeaderCell>
            </Table.Row>
          </Table.Header>

          <Table.Body>
            {data.map((data, index) => this.renderTableRow(data, index))}
          </Table.Body>
        </Table>
      </Segment>
    ) : (
      <Message
        success
        header="The model has not been evaluated"
        content="Evaluate the model before comparing."
      />
    );
  }
}

CompareModels.propTypes = {
  report1: PropTypes.object.isRequired,
  report2: PropTypes.object.isRequired,
  model1: PropTypes.string.isRequired,
  model2: PropTypes.string.isRequired,
  labelType: PropTypes.string.isRequired,
};
