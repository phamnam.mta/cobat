import React from "react";
import PropTypes from "prop-types";
import Alert from "react-s-alert";
import { connect } from 'react-redux';

import requiredIf from "react-required-if";
import { Meteor } from "meteor/meteor";
import { withTracker } from "meteor/react-meteor-data";
import { Button, Form, Icon, Message, Tab } from "semantic-ui-react";
import { activityQuery } from "../activity/queries";
import apolloClient from "../../../../startup/client/apollo";
import { isEmpty } from "lodash";

import IntentReport from "./IntentReport";
import EntityReport from "./EntityReport";
import { InputButtons } from "./InputButtons.jsx";
import { Evaluations } from "../../../../api/nlu_evaluation";
import UploadDropzone from "../../utils/UploadDropzone";
import { Loading } from "../../utils/Utils";

import "react-select/dist/react-select.css";

class Evaluation extends React.Component {
  constructor(props) {
    super(props);
    const { canUpload, evaluation, initialState, validationRender } = props;

    let defaultSelection = 0;
    if (validationRender()) defaultSelection = 3;

    this.state = {
      evaluation,
      data: {},
      contentType: 'application/json',
      loading: false,
      evaluating: false,
      exampleSet: "test",
      errorMessage: null,
      selectedIndex: defaultSelection,
      ...initialState,
      canUpload: canUpload,
    };

    this.evaluate = this.evaluate.bind(this);
    this.loadData = this.loadData.bind(this);
  }

  componentWillReceiveProps(props) {
    const { evaluation } = props;
    this.setState({ evaluation });
  }

  getPrimaryPanes() {
    const {
      evaluation: {
        results: {
          intent_evaluation: intentEvaluation,
          entity_evaluation: entityEvaluation,
        } = {},
      } = {},
    } = this.state;

    const menuItems = [];
    if (intentEvaluation) {
      menuItems.push({
        menuItem: "Intents",
        render: () => <IntentReport {...intentEvaluation} />,
      });
    }
    if (
      entityEvaluation &&
      Object.keys(entityEvaluation).length > 0 &&
      !isEmpty(entityEvaluation["report"])
    ) {
      menuItems.push({
        menuItem: "Entities",
        render: () => <EntityReport {...entityEvaluation} />,
      });
    }

    return menuItems;
  }

  evaluate() {
    this.setState({ evaluating: true });
    const { projectId, workingLanguage, modelId, modelVersion } = this.props;

    const { data, contentType, exampleSet } = this.state;
    if (!modelVersion) {
      Alert.warning("Train or activate the model before evaluation", {
        position: "top-right",
        timeout: 5000,
      });
      this.setState({ evaluating: false });
    } else {
      const useTestingData = exampleSet === 'test' ? true : false
      Meteor.call(
        "rasa.evaluate.nlu",
        modelId,
        modelVersion,
        projectId,
        workingLanguage,
        data,
        contentType,
        useTestingData,
        (err) => {
          this.setState({ evaluating: false });
          if (err) {
            Alert.error(`Error: ${JSON.stringify(err.reason)}`, {
              position: "top-right",
              timeout: "none",
            });
          }
        }
      );
    }
  }

  useUploadTestSet() {
    this.changeExampleSet("uploadTest", true);
  }

  useTrainingSet() {
    this.changeExampleSet("train");
  }

  useTestingSet() {
    this.changeExampleSet("test");
  }

  async useValidatedSet() {
    this.changeExampleSet('validation', true);
    const { projectId, workingLanguage: language } = this.props;
    const { data: { getActivity: { activity: examples } } } = await apolloClient.query({
        query: activityQuery,
        variables: {
            projectId, language, validated: true, pageSize: 0,
        },
    });
    const validExamples = examples
      .filter(({ validated }) => validated)
      .map(({ text, intent, entities }) => {
        entities.forEach(element => Object.keys(element).forEach((key) => element[key] == null && delete element[key]));
        return { text, intent, entities }
      });
    
    // Check that there are nonzero validated examples
    if (validExamples.length > 0) {
        this.setState({
            data: { rasa_nlu_data: { common_examples: validExamples, entity_synonyms: [], gazette: [] } },
            loading: false,
        });
    } else {
        const message = (
            <Message warning>
                <Message.Header>No validated examples</Message.Header>
                <p>See the activity section to manage incoming traffic to this model</p>
            </Message>
        );
        this.setState({ errorMessage: message, loading: false });
    }
}

  changeExampleSet(exampleSet, loading = false) {
    this.setState({
      exampleSet,
      loading,
      data: {},
      errorMessage: null,
    });
  }

  loadData(data, type) {
    const { loading } = this.state;
    try {
      //const parsed = JSON.parse(data);
      if (loading) this.setState({ data: data, contentType: type, loading: false });
    } catch (e) {
      Alert.error(
        "Error: you must upload a JSON, MD or YML file with the same format as an export",
        {
          position: "top-right",
          timeout: "none",
        }
      );
    }
  }

  onError(String) {
    Alert.error(
      String,
      {
        position: "top-right",
        timeout: "none",
      }
    );
  }

  render() {
    const { validationRender, evaluation, loading: reportLoading } = this.props;

    const {
      data,
      exampleSet,
      errorMessage,
      evaluating,
      loading: dataLoading,
      selectedIndex,
      canUpload,
    } = this.state;

    let defaultSelection = 0;
    if (validationRender()) {
      defaultSelection = 2;
    }

    return (
      <Tab.Pane textAlign="center">
        <Loading loading={reportLoading}>
          {errorMessage}
          <br />
          <Form>
            {canUpload && (
              <div id="test_set_buttons">
                <InputButtons
                  labels={[
                    "Use testing set",
                    "Use training set",
                    "Upload test set",
                    "Use validated examples",
                  ]}
                  operations={[
                    this.useTestingSet.bind(this),
                    this.useTrainingSet.bind(this),
                    this.useUploadTestSet.bind(this),
                    this.useValidatedSet.bind(this),
                  ]}
                  defaultSelection={defaultSelection}
                  onDefaultLoad={
                    defaultSelection === 2 ? this.evaluate : () => {}
                  }
                  selectedIndex={selectedIndex}
                />
              </div>
            )}
            {canUpload && exampleSet === "uploadTest" && (
              <UploadDropzone
                accept='md,json,yml'
                onError={this.onError}
                success={!!data}
                onDropped={this.loadData}
                binary={false}
              />
            )}
            {canUpload && !dataLoading && !errorMessage && (
              <div>
                <Button
                  type="submit"
                  basic
                  fluid
                  color="green"
                  loading={evaluating}
                  onClick={this.evaluate}
                  data-cy="start-evaluation"
                >
                  <Icon name="percent" />
                  Start evaluation
                </Button>
                <br />
              </div>
            )}

            {!!evaluation && !evaluating && (
              <Tab
                menu={{ pointing: true, secondary: true }}
                panes={this.getPrimaryPanes()}
              />
            )}
          </Form>
        </Loading>
      </Tab.Pane>
    );
  }
}

Evaluation.propTypes = {
  modelId: PropTypes.string.isRequired,
  modelVersion: PropTypes.string.isRequired,
  evaluation: PropTypes.object,
  projectId: PropTypes.string.isRequired,
  workingLanguage: PropTypes.string.isRequired,
  loading: PropTypes.bool.isRequired,
  validationRender: PropTypes.func,
  initialState: PropTypes.object,
  canUpload: PropTypes.bool.isRequired,
};

Evaluation.defaultProps = {
  validationRender: () => false,
  evaluation: undefined,
  initialState: {},
  canUpload: true,
};

const EvaluationContainer = withTracker((props) => {
  const {
    model: { _id: modelId, versions: versions } = {},
    projectId,
    workingLanguage,
    validationRender,
  } = props;

  const evalsHandler = Meteor.subscribe("nlu_evaluations", modelId);

  let modelVersion = versions.find((x) => x.status === true);
  modelVersion = !modelVersion ? "" : modelVersion._id;

  const evaluation = Evaluations.findOne({ modelId, modelVersion });
  return {
    modelId,
    modelVersion,
    projectId,
    workingLanguage,
    validationRender,
    evaluation,
    loading: !evalsHandler.ready(),
  };
})(Evaluation);

const mapStateToProps = state => ({
  workingLanguage: state.settings.get('workingLanguage'),
  projectId: state.settings.get('projectId'),
});

export default connect(mapStateToProps)(EvaluationContainer);
