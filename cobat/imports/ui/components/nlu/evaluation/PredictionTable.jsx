import React from "react";
import PropTypes from "prop-types";
import matchSorter from "match-sorter";
import ReactTable from "react-table-v6";
import { Label, Popup } from "semantic-ui-react";
import { isEmpty } from "lodash";

export default function PredictionTable(props) {
  const { labelType, predictions } = props;

  const getPredictionsData = () => {
    return predictions.filter((pred) => pred.predicted !== pred[labelType]);
  };

  const getPredictionsColumns = () => {
    const columns = [
      // {
      //   accessor: "token_n_score",
      //   Header: "Word Score",
      //   Cell: (p) => {
      //     return (
      //       <div>
      //         {Object.entries(p.value).map(([key, value]) => (
      //           <span>{`${key}: ${Math.round(value * 100) / 100}, `}</span>
      //         ))}
      //       </div>
      //     );
      //   },
      //   className: "left",
      // },
      {
        id: labelType,
        accessor: (r) => r[labelType],
        Header: `Correct ${labelType}`,
        filterMethod: (filter, rows) =>
          matchSorter(rows, filter.value, { keys: [labelType] }),
        Cell: (p) => (
          <div>
            <Label basic>{p.value}</Label>
          </div>
        ),
        filterAll: true,
        width: 200,
      },
      {
        id: "predicted",
        accessor: (r) => r.predicted,
        Header: "Predicted intent",
        filterMethod: (filter, rows) =>
          matchSorter(rows, filter.value, { keys: ["predicted"] }),
        Cell: (p) => (
          <div>
            {p.original.predicted === p.original.intent && (
              <div>
                <Label basic>{p.value}</Label>
              </div>
            )}
            {p.original.predicted !== p.original.intent && (
              <div>
                <Label basic color="red">
                  {p.value ? p.value : "None"}
                </Label>
              </div>
            )}
          </div>
        ),
        filterAll: true,
        width: 200,
      },
      {
        id: "equal",
        accessor: (e) => (e.intent === e.predicted ? 1 : 0),
        show: false,
      },
      {
        id: "confidence",
        accessor: (p) => Math.round(p.confidence * 100) / 100,
        Header: "Conf.",
        resizable: false,
        width: 60,
      },
    ];
    if (
      !predictions[0]["token_n_score"] ||
      isEmpty(predictions[0]["token_n_score"])
    ) {
      columns.unshift({
        accessor: "text",
        Header: "Text",
        Cell: (p) => <div>{p.value}</div>,
        className: "left",
      });
    } else {
      columns.unshift({
        accessor: "token_n_score",
        Header: "Text",
        Cell: (p) => {
          return (
            <div>
              {Object.entries(p.value).map(([key, value]) => (
                <Popup
                  trigger={
                    <span
                      style={{
                        backgroundColor: `rgba(${value < 0 ? 255 : 0}, ${
                          value >= 0 ? 128 : 0
                        }, 0, ${Math.abs(value)})`,
                      }}
                    >{`${key} `}</span>
                  }
                  content={Math.round(value * 100) / 100}
                />
              ))}
            </div>
          );
        },
        className: "left",
      });
    }
    return columns;
  };

  return (
    <ReactTable data={getPredictionsData()} columns={getPredictionsColumns()} />
  );
}

PredictionTable.propTypes = {
  predictions: PropTypes.array.isRequired,
  labelType: PropTypes.string.isRequired,
};
