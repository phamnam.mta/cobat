import React from 'react';
import PropTypes from 'prop-types';
import matchSorter from 'match-sorter';
import ReactTable from 'react-table-v6';
import { Popup, Icon } from 'semantic-ui-react';
import { getReportData } from '../../../../lib/utils'

export default function ReportTable(props) {
    const { labelType } = props;

    const getReport = () => {
        const { report } = props;
        return getReportData(report, labelType);
    };

    const getReportColumns = () => [
        {
            accessor: labelType,
            Header: labelType.charAt(0).toUpperCase() + labelType.slice(1),
            filterMethod: (filter, rows) => matchSorter(rows, filter.value, { keys: [labelType] }),
            className: 'left',
            filterAll: true,
        },
        {
            id: 'f1-score',
            accessor: (t) => Math.round(t['f1-score'] * 100) / 100,
            Header: () => (
                <div>
                    F1-Score{' '}
                    <Popup
                        trigger={<Icon name='question circle' color='grey' />}
                        content='A general measure of the quality of your model based on precision and accuracy'
                    />
                </div>
            ),
            filterable: false,
            width: 100,
        },
        {
            id: 'precision',
            accessor: (t) => Math.round(t['precision'] * 100) / 100,
            Header: () => (
                <div>
                    Precision{' '}
                    <Popup
                        trigger={<Icon name='question circle' color='grey' />}
                        content='On 100 predictions for label, how many were actually labeled as such in test set'
                    />
                </div>
            ),
            filterable: false,
            width: 100,
        },
        {
            id: 'recall',
            accessor: (t) => Math.round(t['recall'] * 100) / 100,
            Header: () => (
                <div>
                    Recall{' '}
                    <Popup
                        trigger={<Icon name='question circle' color='grey' />}
                        content='On 100 instances of label in test set, how many were actually predicted'
                    />
                </div>
            ),
            filterable: false,
            width: 100,
        },
        {
            accessor: 'support',
            Header: () => (
                <div>
                    Support{' '}
                    <Popup
                        trigger={<Icon name='question circle' color='grey' />}
                        content='The number of examples for that label'
                    />
                </div>
            ),
            filterable: false,
            width: 100,
        },
    ];

    return (
        <ReactTable
            data={getReport()}
            filterable
            columns={getReportColumns()}
            minRows={1}
            SubComponent={null}
        />
    );
}

ReportTable.propTypes = {
    report: PropTypes.object.isRequired,
    labelType: PropTypes.string.isRequired,
};
