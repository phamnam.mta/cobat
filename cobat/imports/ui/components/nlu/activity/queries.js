import gql from 'graphql-tag';

export const activityFields = gql`
    fragment ActivityFields on Activity {
        _id,
        text,
        intent,
        entities {
            entity,
            value,
            group,
            role,
            start,
            end,
            confidence,
            extractor
        },
        confidence,
        validated,
        createdAt,
        updatedAt
    }
`;

export const activityQuery = gql`
    query (
        $projectId: String!
        $language: String!
        $sortKey: String = "updatedAt"
        $sortDesc: Boolean = true
        $pageSize: Int = 20
        $cursor: String
        $validated: Boolean = false
        $filter: FilterInput
    ) {
        getActivity(
            projectId: $projectId
            language: $language
            sortKey: $sortKey
            sortDesc: $sortDesc
            pageSize: $pageSize
            cursor: $cursor
            validated: $validated
            filter: $filter
        ) {
            activity {
                ...ActivityFields
            },
            pageInfo {
                endCursor
                hasNextPage
            }
        }
    }
    ${activityFields}
`;

export const getActivityCount = gql`
  query ($projectId: String!, $language: String!) {
    getActivity(projectId: $projectId, language: $language) {
      pageInfo {
        totalLength
      }
    }
  }
`;

export const activityChanged = gql`
  subscription (
    $projectId: String!
    $language: String!
  ) {
    activityChanged(projectId: $projectId, language: $language) {
      changed
    }
  }
`;
