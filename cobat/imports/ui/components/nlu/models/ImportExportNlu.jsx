import React, { useState, useContext } from "react";
import PropTypes from "prop-types";
import Alert from 'react-s-alert';
import {
  Button,
  Icon,
  Message,
  Tab,
  Form,
  Checkbox,
  Grid,
  Menu,
  Segment,
} from "semantic-ui-react";
import "brace/mode/json";
import "brace/theme/github";
import { saveAs } from "file-saver";
import moment from "moment";
import { ProjectContext } from "../../../layouts/context";
import { wrapMeteorCallback } from "../../utils/Errors";
import UploadDropzone from "../../utils/UploadDropzone";
import { tooltipWrapper } from '../../utils/Utils';

function ImportExportNlu(props) {
  const { language, projectId } = props;
  const { projectLanguages } = useContext(ProjectContext);
  
  const [backupDownloaded, setBackupDownloaded] = useState(false);
  const [format, setFormat] = useState('json');
  const [status, setStatus] = useState(-1);
  const [isTestingData, setIsTestingData] = useState(false);
  const [loading, setLoading] = useState(false);
  const [activeMenuItem, setActiveMenuItem] = useState('Import');
  const [data, setData] = useState('');
  const [overwriteOnSameText, setOverwriteOnSameText] = useState(true);
  const [importToTestSet, setImportToTestSet] = useState(false);
  const [wipeInvolvedCollections, setWipeInvolvedCollections] = useState(false);
  const [downloadBackup, setDownloadBackup] = useState(true);

  const downloadModelData = () => {
    if (window.Cypress) {
      setBackupDownloaded(true);
      return;
    }
    
    const forCobat = format === "cobat"
    setLoading(true);
    Meteor.call(
      "rasa.getTrainingPayload",
      projectId,
      { language },
      status,
      forCobat,
      isTestingData,
      wrapMeteorCallback((_, res) => {
        const { [language]: data } = res.nlu;
        if (format === "json" || format === "cobat") {
          const blob = new Blob([JSON.stringify(data)], {
            type: "application/json",
          });
          const filename = `${projectId.toLowerCase()}-${language}-${moment().toISOString()}.json`;
          saveAs(blob, filename);
          setBackupDownloaded(true );
          setLoading(false);
        } else {
          Meteor.call(
            "rasa.data.convert",
            data,
            projectId,
            language,
            format,
            wrapMeteorCallback((_, res) => {
              const blob = new Blob([res], {
                type: "text/plain;charset=utf-8",
              });
              const filename = `${projectId.toLowerCase()}-${language}-${moment().toISOString()}.${format}`;
              saveAs(blob, filename);
              setBackupDownloaded(true);
              setLoading(false);
            })
          );
        }
      })
    );
  };

  const loadData = (data, type) => {
    try {
      if (type === 'application/json') {
        const parsed = JSON.parse(data);
        setData(parsed);
      }
      else {
        const extension = type === 'text/markdown' ? 'md' : 'yaml';
        setLoading(true);
        Meteor.call(
          "rasa.convertNluToJson",
          data,
          extension,
          projectId,
          language,
          wrapMeteorCallback((_, res) => {
            if (!res) {
              Alert.error(
                "Error: data format not validated",
                {
                  position: "top-right",
                  timeout: "none",
                }
              );
              return;
            }
            setData(res);
            setLoading(false);
          })
        );
      }
    } catch (e) {
      Alert.error(
        "Error: you must upload a JSON, MD or YML file with the same format as an export",
        {
          position: "top-right",
          timeout: "none",
        }
      );
    }
  }

  const onError = (String) => {
    Alert.error(
      String,
      {
        position: "top-right",
        timeout: "none",
      }
    );
  }

  const handleChangeDataFormat = (e, { value }) => setFormat(value);
  const handleChangeDataStatus = (e, { value }) => setStatus(value);

  const handleImport = ({
    rasa_nlu_data: {
        common_examples: commonExamples = [],
        entity_synonyms: synonyms = [],
        gazette = [],
        regex_features: regex = [],
    } = {}
  }) => {
    const examples = commonExamples.map((obj) => ({
      ...obj,
      metadata: { language },
    }));
    setLoading(true);
    if (downloadBackup) { 
      Meteor.call(
        "rasa.getTrainingPayload",
        projectId,
        { language },
        status,
        true,
        importToTestSet,
        wrapMeteorCallback((_, res) => {
          const { [language]: data } = res.nlu;
          const blob = new Blob([JSON.stringify(data)], {
            type: "application/json",
          });
          const filename = `${projectId.toLowerCase()}-${language}-${moment().toISOString()}.json`;
          saveAs(blob, filename);
        })
      );
    }
    Meteor.call(
      "nlu.importExamples",
      projectId,
      language,
      examples,
      overwriteOnSameText,
      importToTestSet,
      wipeInvolvedCollections,
      wrapMeteorCallback((_, res) => {
        if (!res) {
          Alert.error(
            "Error occurred while inserting example",
            {
              position: "top-right",
              timeout: "none",
            }
          );
        }
        setData('');
        setLoading(false);
      })
    );
  };

  const renderMenuItem = (itemText, itemKey = itemText) => {
    return (
        <Menu.Item
            key={itemKey}
            active={activeMenuItem === itemKey}
            onClick={() => { setActiveMenuItem(itemKey); }}
        >
            {itemText}
        </Menu.Item>
    );
  };

  const warnWipe = () => {
    let message = null;
    if (overwriteOnSameText) message = 'Overwrite existing examples is enabled';
    if (wipeInvolvedCollections) message = 'Delete existing data is enabled';
    if (message) {
        return (
            <Message warning>
                <Message.Header>Wipe on import</Message.Header>
                {message}
            </Message>
        );
    }
    return null;
  };

  const renderImportBottom = () => {
    return (
    <>
        {warnWipe()}
        <Message info>
            <Message.Header>Import summary</Message.Header>
            <Message.List className='import-summary-accordion'>
              <Message.Item>{`${data?.rasa_nlu_data?.common_examples?.length} examples will be imported.`}</Message.Item>
            </Message.List>
            <br />
            <Checkbox
                    toggle
                    className='download-backup'
                    checked={downloadBackup}
                    onChange={() => setDownloadBackup(!downloadBackup)}
                    label='Download backup before the import'
                />
                <br />
            <Button
                disabled={!data}
                content='Import'
                loading={loading}
                primary
                onClick={() => handleImport(data)}
            />
            <Button
            style={{marginLeft: '10px'}}
                content='Cancel'
                onClick={() => setData('')}
            />
        </Message>
    </>
  )};

  const renderImportNlu = () => {
    return (<>
      <Segment>
        <div className='wipes side-by-side left'>
            {tooltipWrapper(
                <Checkbox
                    toggle
                    checked={overwriteOnSameText}
                    onChange={() => {
                        setOverwriteOnSameText(!overwriteOnSameText);
                    }}
                    label='Overwrite existing examples'
                    data-cy='wipe-data'
                />,
                `This will overwrite examples on same text.`,
            )}
            {tooltipWrapper(
                <Checkbox
                toggle
                checked={importToTestSet}
                onChange={() => {
                  setImportToTestSet(!importToTestSet);
                }}
                label='Import to test set'
                data-cy='wipe-project'
            />,
                `With this switch on data will be imported to the test set.`,
            )}
            {tooltipWrapper(
                    <Checkbox
                        toggle
                        checked={wipeInvolvedCollections}
                        onChange={() => setWipeInvolvedCollections(!wipeInvolvedCollections)}
                        label='Delete existing data'
                    />,
                    `This will clear the existing data for the type of data you are importing.`,
            )}
            
        </div>
      {!!data ? (renderImportBottom()) : (<UploadDropzone
      loading={loading}
      accept='md,json,yml'
      onError={onError}
      onDropped={loadData}
      binary={false}
      maxSizeInMb={10}
    />)}
    </Segment>

    </>)
  }

  const renderExportNlu = () => {
    const { text: languageName } = projectLanguages.find(
      (lang) => lang.value === language
    );
    return (
      <Tab.Pane>
        {!backupDownloaded && (
          <div>
            <Grid>
              <Grid.Row>
                <Grid.Column width={6}>
                  <Form>
                    <Form.Field>
                      <Checkbox
                        radio
                        label="Rasa json format"
                        name="checkboxRadioGroup"
                        value="json"
                        checked={format === "json"}
                        onChange={handleChangeDataFormat}
                      />
                    </Form.Field>
                    <Form.Field>
                      <Checkbox
                        radio
                        label="Rasa yaml format"
                        name="checkboxRadioGroup"
                        value="yml"
                        checked={format === "yml"}
                        onChange={handleChangeDataFormat}
                      />
                    </Form.Field>
                    <Form.Field>
                      <Checkbox
                        radio
                        label="Rasa markdown format"
                        name="checkboxRadioGroup"
                        value="md"
                        checked={format === "md"}
                        onChange={handleChangeDataFormat}
                      />
                    </Form.Field>
                    <Form.Field>
                      <Checkbox
                        radio
                        label="CoBAT format"
                        name="checkboxRadioGroup"
                        value="cobat"
                        checked={format === "cobat"}
                        onChange={handleChangeDataFormat}
                      />
                    </Form.Field>
                  </Form>
                </Grid.Column>
                <Grid.Column width={5}>
                  <Form>
                    <Form.Field>
                      <Checkbox
                        radio
                        label="All data"
                        name="checkboxRadioGroup"
                        value={-1}
                        checked={status === -1}
                        onChange={handleChangeDataStatus}
                      />
                    </Form.Field>
                    <Form.Field>
                      <Checkbox
                        radio
                        label="Validated"
                        name="checkboxRadioGroup"
                        value={1}
                        checked={status === 1}
                        onChange={handleChangeDataStatus}
                      />
                    </Form.Field>
                    <Form.Field>
                      <Checkbox
                        radio
                        label="Not validated"
                        name="checkboxRadioGroup"
                        value={0}
                        checked={status === 0}
                        onChange={handleChangeDataStatus}
                      />
                    </Form.Field>
                  </Form>
                </Grid.Column>
                <Grid.Column width={5}>
                  <Form>
                    <Form.Field>
                      <Checkbox
                        radio
                        label="Train set"
                        name="checkboxRadioGroup"
                        checked={!isTestingData}
                        onChange={() => setIsTestingData(false)}
                      />
                    </Form.Field>
                    <Form.Field>
                      <Checkbox
                        radio
                        label="Test set"
                        name="checkboxRadioGroup"
                        checked={isTestingData}
                        onChange={() => setIsTestingData(true)}
                      />
                    </Form.Field>
                  </Form>
                </Grid.Column>
              </Grid.Row>
            </Grid>
            <br />
            <br />
            <Button
              loading={loading}
              disabled={loading}
              positive
              onClick={downloadModelData}
              className="dowload-model-backup-button"
              data-cy="download-backup"
            >
              <Icon name="download" />
              Export {languageName} data of your model
            </Button>
          </div>
        )}
        {backupDownloaded && (
          <Message success icon="check circle" content="Backup downloaded" />
        )}
        <br />
        <br />
      </Tab.Pane>
    );
  }

  const getMenuPanes = () => {
    return [
        {
            menuItem: renderMenuItem('Import'),
            render: renderImportNlu,
        },
        {
            menuItem: renderMenuItem('Export'),
            render: renderExportNlu,
        },
    ];
  }

  return (
    <Tab menu={{ vertical: true }} grid={{ paneWidth: 13, tabWidth: 3 }} panes={getMenuPanes()} />
  );
}

ImportExportNlu.propTypes = {
  projectId: PropTypes.string.isRequired,
  language: PropTypes.string.isRequired,
};

export default ImportExportNlu;
