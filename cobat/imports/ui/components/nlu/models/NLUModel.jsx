import React, {
  useState,
  useCallback,
  useEffect,
  useContext,
  useRef,
  useMemo,
} from "react";
import Alert from "react-s-alert";
import PropTypes from "prop-types";
import { Meteor } from "meteor/meteor";
import { browserHistory } from "react-router";
import { useTracker } from "meteor/react-meteor-data";
import { Container, Icon, Menu, Message, Tab } from "semantic-ui-react";
import "react-select/dist/react-select.css";
import { connect } from "react-redux";
import { debounce } from "lodash";
import { PageMenu } from "../../utils/Utils";
import { cleanDucklingFromExamples } from "../../../../lib/utils";
import { can } from "../../../../lib/scopes";
import { NLUModels } from "../../../../api/nlu_model/nlu_model.collection";
import InsertNlu from "../../example_editor/InsertNLU";
import Evaluation from "../evaluation/Evaluation";
import ChitChat from "./ChitChat";
import Synonyms from "../../synonyms/Synonyms";
import Gazette from "../../synonyms/Gazette";
import RegexFeatures from "../../synonyms/RegexFeatures";
import NLUPipeline from "./settings/NLUPipeline";
import Statistics from "./Statistics";
import ModelsTable from "../versions/ModelsTable";
import DeleteModel from "./DeleteModel";
import ImportExportNlu from "./ImportExportNlu";
import IntentBulkInsert from "./IntentBulkInsert";
import { clearTypenameField } from "../../../../lib/client.safe.utils";
import LanguageDropdown from "../../common/LanguageDropdown";
import API from "./API";
import { setWorkingLanguage } from "../../../store/actions/actions";
import NluTable from "./NluTable";
import NluDataTable from "./NluDataTable";
import { ProjectContext } from "../../../layouts/context";
import {
  useExamples,
  useDeleteExamples,
  useUpdateExamples,
  useSwitchCanonical,
  useInsertExamples,
  useAssignExamples,
} from "./hooks";

function NLUModel(props) {
  const { changeWorkingLanguage } = props;
  const { project, instance, intents, entities } = useContext(ProjectContext);
  const {
    location: { state: incomingState },
    params: { language: langFromParams, project_id: projectId } = {},
    workingLanguage,
  } = props;

  if (workingLanguage !== langFromParams) {
    browserHistory.push({
      pathname: `/project/${projectId}/nlu/model/${workingLanguage}`,
    });
  }

  const { model } = useTracker(() => {
    Meteor.subscribe("nlu_models", projectId);
    return {
      model: NLUModels.findOne({ projectId, language: workingLanguage }),
    };
  });
  
  const isManager = can("manager", projectId);

  const [filters, setFilters] = useState({
    sortKey: "createdAt",
    order: "DESC",
    status: -1,
    userIdFilter: (isManager || can("viewer", projectId)) ? "" : Meteor.userId(),
  });
  
  const canNLUEdit = isManager || (can("nlu-data:w", projectId) && filters.userIdFilter === Meteor.userId());
  const canModelEdit = can("nlu-model:w", projectId);

  const [pagination, setPagination] = useState({ pageSize: 20, pageIndex: -1 });

  const variables = useMemo(
    () => ({
      ...filters,
      ...pagination,
      projectId,
      language: workingLanguage,
    }),
    [pagination, filters, projectId, workingLanguage]
  );
  const tableRef = useRef();

  const {
    data,
    loading: loadingExamples,
    doing,
    hasNextPage,
    totalLength,
    loadMore,
    refetch,
  } = useExamples(variables);

  const [deleteExamples] = useDeleteExamples(variables);
  const [switchCanonical] = useSwitchCanonical(variables);
  const [updateExamples] = useUpdateExamples(variables);
  const [insertExamples] = useInsertExamples(variables);
  const [assignExamples] = useAssignExamples(variables);
  const [selection, setSelection] = useState([]);

  const [activityLinkRender, setActivityLinkRender] = useState(
    (incomingState && incomingState.isActivityLinkRender) || false
  );
  const [activeItem, setActiveItem] = useState(
    incomingState && incomingState.isActivityLinkRender === true
      ? "Evaluation"
      : "Training Data"
  );

  const validationRender = () => {
    if (activityLinkRender === true) {
      setActivityLinkRender(false);
      return true;
    }
    return false;
  };
  // if we do not useCallback the debounce is re-created on every render
  const setFiltersDebounced = useCallback(
    debounce((newFilters) => {
      setFilters({
        ...filters,
        intents: newFilters.intents,
        entities: newFilters.entities,
        onlyCanonicals: newFilters.onlyCanonicals,
        isTestingData: newFilters.isTestingData,
        text: [newFilters.query],
        userIdFilter: newFilters.userIdFilter,
        status: newFilters.status,
        user: newFilters.user,
        order: newFilters.order,
        sortKey: newFilters.sortKey,
      });
    }, 500),
    []
  );

  useEffect(() => {
    if (refetch) refetch();
  }, [variables]);

  const handleLanguageChange = (value) => {
    changeWorkingLanguage(value);
    browserHistory.push({
      pathname: `/project/${projectId}/nlu/model/${value}`,
    });
  };

  const handleMenuItemClick = (e, { name }) => setActiveItem(name);

  const renderWarningMessageIntents = () => {
    if (!loadingExamples && intents.length < 2) {
      return (
        <Message
          size="tiny"
          content={
            <div>
              <Icon name="warning" />
              You need at least two distinct intents to train NLU
            </div>
          }
          info
        />
      );
    }
    return <></>;
  };

  const handleInsert = (examples) => {
    const cleanedExamples = cleanDucklingFromExamples(examples);
    insertExamples({
      variables: {
        examples: cleanedExamples,
        language: workingLanguage,
        projectId,
        options: { autoAssignCanonical: false }
      },
    }).then(() => tableRef?.current?.scrollToItem(0));
  };

  const renderTopMenuItem = (name, icon, visible) =>
    !visible ? null : (
      <Menu.Item
        key={name}
        name={name}
        active={activeItem === name}
        onClick={handleMenuItemClick}
        data-cy={`nlu-menu-${name.replace(/ /g, "-").toLowerCase()}`}
      >
        <Icon size="small" name={icon} />
        {name}
      </Menu.Item>
    );

  const topMenuItems = [
    ["Training Data", "database", true],
    ["Evaluation", "percent", true],
    ["Statistics", "pie graph", true],
    ["Settings", "setting", canModelEdit],
    ["Versions", "list", canModelEdit],
  ];

  const renderTopMenu = () => (
    <PageMenu mode="nlu">
      <Menu.Item header>
        <LanguageDropdown handleLanguageChange={handleLanguageChange} />
      </Menu.Item>
      {topMenuItems.map(([...params]) => renderTopMenuItem(...params))}
    </PageMenu>
  );

  if (!project) return null;
  if (!model) return null;

  return (
    <>
      {renderTopMenu()}
      <Container data-cy="nlu-page">
        {isManager && activeItem === "Training Data" && (
          <>
            {renderWarningMessageIntents()}
            <br />
            <InsertNlu onSave={handleInsert} skipDraft={true} />
          </>
        )}
        <br />
        {activeItem === "Training Data" && (
          <Tab
            menu={{ pointing: true, secondary: true }}
            // activeIndex === 0 is the example tab, we want to refetch data everytime we land on it
            // as it may have changed from the chitchat tab
            onTabChange={(e, { activeIndex }) => {
              if (activeIndex === 0) refetch();
            }}
            panes={[
              {
                menuItem: "Examples",
                render: () => (
                  <NluDataTable
                    canEdit={canNLUEdit}
                    isManager={isManager}
                    onEditExample={(examples) =>
                      updateExamples({
                        variables: {
                          examples,
                          projectId,
                          language: workingLanguage,
                        },
                      })
                    }
                    onDeleteExample={(ids) =>
                      deleteExamples({
                        variables: {
                          ids,
                          projectId,
                          language: workingLanguage,
                        },
                      })
                    }
                    onSwitchCanonical={(example) =>
                      switchCanonical({
                        variables: {
                          projectId,
                          language: workingLanguage,
                          example: clearTypenameField(example),
                        },
                      })
                    }
                    getMoreExamples={(numberExamples) => {
                      Promise.resolve(
                        assignExamples({
                          variables: {
                            projectId,
                            language: workingLanguage,
                            intents: filters.intents || [],
                            entities: filters.entities || [],
                            numberExamples: numberExamples,
                          },
                        })
                      ).then((res) => {
                        if (!res) {
                          Alert.error("Error occurred while deleting example", {
                            position: "top-right",
                            timeout: "none",
                          });
                          return;
                        }
                        if (refetch) refetch();
                      });
                    }}
                    examples={data}
                    updateFilters={setFiltersDebounced}
                    filters={filters}
                    doing={doing}
                    pages={Math.ceil(totalLength / pagination.pageSize)}
                    updatePagination={(pageSize, pageIndex) =>
                      setPagination({ pageSize, pageIndex })
                    }
                  />
                ),
              },
              {
                menuItem: "Synonyms",
                render: () => <Synonyms model={model} />,
              },
              {
                menuItem: "API",
                render: () => <API model={model} instance={instance} />,
              },
            ]}
          />
        )}
        {activeItem === "Evaluation" && (
          <Evaluation
            canUpload={canModelEdit}
            model={model}
            projectId={projectId}
            validationRender={validationRender}
          />
        )}
        {activeItem === "Statistics" && (
          <Statistics
            synonyms={model.training_data.entity_synonyms.length}
            gazettes={model.training_data.fuzzy_gazette.length}
            intents={intents}
            entities={entities}
          />
        )}
        {activeItem === "Settings" && (
          <Tab
            menu={{ pointing: true, secondary: true }}
            panes={[
              {
                menuItem: "Import/Export",
                render: () => <ImportExportNlu projectId={projectId} language={workingLanguage}/>,
              },
              {
                menuItem: "Pipeline",
                render: () => (
                  <NLUPipeline model={model} projectId={projectId} />
                ),
              },
              {
                menuItem: "Delete",
                render: () => <DeleteModel />,
              },
            ]}
          />
        )}
        {activeItem === "Versions" && <ModelsTable model={model} />}
      </Container>
    </>
  );
}

NLUModel.propTypes = {
  params: PropTypes.object.isRequired,
  location: PropTypes.object.isRequired,
  workingLanguage: PropTypes.string,
  changeWorkingLanguage: PropTypes.func.isRequired,
};

NLUModel.defaultProps = {
  workingLanguage: null,
};

const mapStateToProps = (state) => ({
  workingLanguage: state.settings.get("workingLanguage"),
});

const mapDispatchToProps = {
  changeWorkingLanguage: setWorkingLanguage,
};

export default connect(mapStateToProps, mapDispatchToProps)(NLUModel);
