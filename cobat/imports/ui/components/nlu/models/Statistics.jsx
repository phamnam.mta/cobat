import React, { useState, useMemo, useEffect } from "react";
import ReactTable from "react-table-v6";
import PropTypes from "prop-types";
import { Statistic, Button, Tab } from "semantic-ui-react";
import { withTracker } from "meteor/react-meteor-data";
import { connect } from "react-redux";
import { useQuery } from "@apollo/react-hooks";
import { saveAs } from "file-saver";
import { Stories as StoriesCollection } from "../../../../api/story/stories.collection";
import { Loading } from "../../utils/Utils";
import IntentLabel from "../common/IntentLabel";
import {
  GET_INTENT_STATISTICS,
  GET_EXAMPLE_COUNT,
  GET_ANNOTATOR_STATISTICS,
} from "./graphql";

const Statistics = (props) => {
  const {
    trainingExamples,
    testingExamples,
    freeExamples,
    synonyms,
    gazettes,
    intents,
    entities,
    storyCount,
    ready,
    projectId,
    workingLanguage,
  } = props;

  const {
    data: exampleStatistic,
    loading: loadingExamples,
    refetch: refetchExamples,
  } = useQuery(GET_INTENT_STATISTICS, {
    variables: { projectId, language: workingLanguage },
  });

  const {
    data: annotatorStatistic,
    loading: loadingAnnotator,
    refetch: refetchAnnotator,
  } = useQuery(GET_ANNOTATOR_STATISTICS, {
    variables: { projectId, language: workingLanguage },
  });

  const [activatePane, setActivatePane] = useState(0);

  // always refetch on first page load
  useEffect(() => {
    if (refetchExamples) refetchExamples();
    if (refetchAnnotator) refetchAnnotator();
  }, [refetchExamples, refetchAnnotator, workingLanguage]);

  const getExampleStatisticsToDisplay = () =>
    !loadingExamples &&
    exampleStatistic.getIntentStatistics.map(({ intent, example, counts }) => {
      const row = { intent, example: example ? example.text : null };
      counts.forEach(({ type, count }) => {
        if (type) {
          row['test'] = count;
        }
        else {
          row['train'] = count;
        }
      });
      return row;
  });

  const getAnnotatorStatisticsToDisplay = () =>
    !loadingAnnotator &&
    annotatorStatistic.getAnnotatorStatistics.map(({ annotator, status }) => ({
      email: annotator && annotator.email ? annotator.email : "",
      name: annotator && annotator.name ? annotator.name : "",
      status,
    })).filter(e => e.email);

  const exampleStatisticsToDisplay = useMemo(
    () => getExampleStatisticsToDisplay(),
    [exampleStatistic]
  );
  const annotatorStatisticsToDisplay = useMemo(
    () => getAnnotatorStatisticsToDisplay(),
    [annotatorStatistic]
  );

  const downloadData = () => {
    const headers = ["intent", "example", "train", "test"];
    const csvData = (exampleStatisticsToDisplay || [])
      .reduce(
        (acc, curr) => {
          let row = "";
          headers.forEach((h) => {
            row += `"${`${curr[h] || ""}`.replace('"', '""')}",`;
          });
          return [...acc, row];
        },
        [headers.map((h) => `"${h}"`)]
      )
      .join("\n");
    const blob = new Blob([csvData], { type: "text/csv;charset=utf-8" });
    return saveAs(
      blob,
      `nlu_statistics_${projectId}_${
        new Date().toISOString().split("T")[0]
      }.csv`
    );
  };

  const renderCards = () => {
    const cards = [
      { label: "Train set", value: trainingExamples },
      { label: "Test set", value: testingExamples },
      { label: "Free", value: freeExamples },
      { label: "Intents", value: intents.length },
      { label: "Entities", value: entities.length },
      { label: "Synonyms", value: synonyms },
    ];

    return cards.map((d) => (
      <div
        className="glow-box"
        style={{ width: `calc(100% / ${cards.length})` }}
        key={d.label}
      >
        <Statistic>
          <Statistic.Label>{d.label}</Statistic.Label>
          <Statistic.Value>{d.value}</Statistic.Value>
        </Statistic>
      </div>
    ));
  };

  const renderIntent = (props) => {
    return (
      <IntentLabel
        value={props.value ? props.value : ""}
        allowEditing={false}
      />
    );
  };

  const renderExample = (props) => {
    if (!props.value) return <i>No example defined.</i>;
    return props.value;
  };

  const exampleColumns = [
    {
      id: "intent",
      accessor: (t) => t.intent,
      Header: "Intent",
      width: 200,
      Cell: renderIntent,
    },
    {
      id: "example",
      accessor: (t) => t.example,
      Header: "Example",
      Cell: renderExample,
    },
    {
      id: "train",
      accessor: (t) => t.train,
      Header: "Train",
      width: 70,
    },
    {
      id: "test",
      accessor: (t) => t.test,
      Header: "Test",
      width: 70,
    },
  ];

  const annotatorColumns = [
    {
      id: "email",
      accessor: (t) => t.email,
      Header: "Annotator",
      width: 200,
      Cell: (props) => {
        return <b>{props.value}</b>;
      },
    },
    {
      id: "name",
      accessor: (t) => t.name,
      Header: "Name",
      width: 200,
      Cell: (props) => {
        return props.value;
      },
    },
    {
      id: "status",
      accessor: (t) => t.status,
      Header: "Validated",
      Cell: (props) => {
        if (!props.value) return 0;
        const { count } = props.value.find((e) => e.value === 1) || {
          count: 0,
        };
        return count;
      },
    },
    {
      id: "status",
      accessor: (t) => t.status,
      Header: "Confused",
      Cell: (props) => {
        if (!props.value) return 0;
        const { count } = props.value.find((e) => e.value === 2) || {
          count: 0,
        };
        return count;
      },
    },
    {
      id: "status",
      accessor: (t) => t.status,
      Header: "Not validated",
      Cell: (props) => {
        if (!props.value) return 0;
        const { count } = props.value.find((e) => e.value === 0) || {
          count: 0,
        };
        return count;
      },
    },
  ];

  return (
    <Loading loading={!ready || loadingExamples || loadingAnnotator}>
      <div className="side-by-side">{renderCards()}</div>
      <br />
      {
        <Tab
          menu={{ pointing: true, secondary: true }}
          defaultActiveIndex={activatePane}
          onTabChange={(e, { activeIndex }) => {
            setActivatePane(activeIndex);
          }}
          panes={[
            exampleStatisticsToDisplay && exampleStatisticsToDisplay.length
              ? {
                  menuItem: "Examples",
                  render: () => (
                    <div className="glow-box extra-padding">
                      <div className="side-by-side">
                        <h3>Examples per intent</h3>
                        <Button
                          onClick={downloadData}
                          disabled={!(exampleStatisticsToDisplay || []).length}
                          icon="download"
                          basic
                        />
                      </div>
                      <br />
                      <ReactTable
                        columns={exampleColumns}
                        data={exampleStatisticsToDisplay}
                        getTheadThProps={() => ({
                          style: {
                            borderRight: "none",
                            textAlign: "left",
                            fontWeight: 800,
                            paddingBottom: "10px",
                          },
                        })}
                      />
                    </div>
                  ),
                }
              : null,
            annotatorStatisticsToDisplay && annotatorStatisticsToDisplay.length
              ? {
                  menuItem: "Annotator",
                  render: () => (
                    <div className="glow-box extra-padding">
                        <br />
                      <ReactTable
                        columns={annotatorColumns}
                        data={annotatorStatisticsToDisplay}
                        getTheadThProps={() => ({
                          style: {
                            borderRight: "none",
                            textAlign: "left",
                            fontWeight: 800,
                            paddingBottom: "10px",
                          },
                        })}
                      />
                    </div>
                  ),
                }
              : null,
          ]}
        />
      }
    </Loading>
  );
};

Statistics.propTypes = {
  trainingExamples: PropTypes.number.isRequired,
  testingExamples: PropTypes.number.isRequired,
  freeExamples: PropTypes.number.isRequired,
  synonyms: PropTypes.number.isRequired,
  gazettes: PropTypes.number.isRequired,
  intents: PropTypes.array.isRequired,
  entities: PropTypes.array.isRequired,
  ready: PropTypes.bool.isRequired,
  storyCount: PropTypes.number.isRequired,
  projectId: PropTypes.string.isRequired,
  workingLanguage: PropTypes.string.isRequired,
};

const StatisticsWithStoryCount = withTracker((props) => {
  const { projectId, workingLanguage: language } = props;
  const storiesHandler = Meteor.subscribe("stories.light", projectId, language);
  const { data } = useQuery(GET_EXAMPLE_COUNT, {
    variables: { projectId, language },
  });
  const { data: dataTest } = useQuery(GET_EXAMPLE_COUNT, {
    variables: { projectId, language, isTestingData: true },
  });
  const { data: freeData } = useQuery(GET_EXAMPLE_COUNT, {
    variables: { projectId, language, userIdFilter: null, status: 0 },
  });
  const { totalLength: trainingExamples = 0 } = data?.examples?.pageInfo || {};
  const { totalLength: testingExamples = 0 } = dataTest?.examples?.pageInfo || {};
  const { totalLength: freeExamples = 0 } = freeData?.examples?.pageInfo || {};

  return {
    ready: storiesHandler.ready(),
    trainingExamples,
    testingExamples,
    freeExamples,
    storyCount: StoriesCollection.find().count(),
  };
})(Statistics);

const mapStateToProps = (state) => ({
  projectId: state.settings.get("projectId"),
  workingLanguage: state.settings.get("workingLanguage"),
});

export default connect(mapStateToProps)(StatisticsWithStoryCount);
