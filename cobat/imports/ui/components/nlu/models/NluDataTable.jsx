import { Meteor } from "meteor/meteor";
import React, { useState, useContext } from "react";
import moment from "moment";
import PropTypes, { number } from "prop-types";
import {
  Checkbox,
  Tab,
  Grid,
  Dropdown,
  Popup,
  Icon,
  Label,
  Confirm,
  Message,
  Button,
} from "semantic-ui-react";
import Alert from "react-s-alert";
import "react-s-alert/dist/s-alert-default.css";
import ReactTable from "react-table-v6";
import matchSorter from "match-sorter";
import getColor from "../../../../lib/getColors";
import { can } from "../../../../lib/scopes";
import IntentLabel from "../common/IntentLabel";
import Filters from "./Filters";
import { ProjectContext } from "../../../layouts/context";
import IconButton from "../../common/IconButton";
import UserUtteranceViewer from "../common/UserUtteranceViewer";
import { ExampleTextEditor } from "../../example_editor/ExampleTextEditor";
import { clearTypenameField } from "../../../../lib/client.safe.utils";

function NluDataTable(props) {
  const {
    examples,
    hideHeader,
    conditionalRowFormatter,
    className,
    onEditExample,
    onDeleteExample,
    onSwitchCanonical,
    getMoreExamples,
    doing,
    filters,
    updateFilters,
    pages,
    updatePagination,
    canEdit,
    isManager,
    users,
    usersOptions,
  } = props;

  const [takingExamplesOpen, setTakingExamplesOpen] = useState(false);
  const [numberExamples, setNumberExamples] = useState(20);
  const [editExampleMode, setEditExampleMode] = useState(null);
  const [clickTimeout, setClickTimeout] = useState(null);

  const [deleteExample, setDeleteExample] = useState({
    confirmOpen: false,
    _id: "",
  });

  const {
    intents,
    entities,
    project: { _id: projectId },
  } = useContext(ProjectContext);
  const isAnnotator = Roles.userIsInRole(
    Meteor.userId(),
    ["annotator"],
    projectId
  );

  const handleEditExample = (example) =>
    onEditExample([clearTypenameField(example)]);

  const getColumns = () => {
    let columns = [
      {
        accessor: "intent",
        Header: "Intent",
        width: 200,
        filterMethod: (filter, rows) =>
          matchSorter(rows, filter.value, { keys: ["intent"] }),
        Cell: (props) => {
          const { metadata: { canonical = false } = {} } = props.row.example;

          return canonicalTooltip(
            <span className="example-intent-cell">
              <IntentLabel
                value={props.value}
                allowEditing={canEdit && !canonical}
                disabled={!canEdit}
                allowAdditions
                onChange={(intent) =>
                  handleEditExample({ ...props.row.example, intent })
                }
              />
            </span>,
            canonical
          );
        },
      },
      {
        id: "example",
        sortMethod: (a, b) => a.text.localeCompare(b.text),
        sortable: true,
        accessor: (e) => e,
        Header: "Example",
        Cell: (props) => {
          const {
            _id: exampleId,
            metadata: { canonical = false } = {},
            updatedAt,
            updatedBy: userId,
          } = props.row.example;
          const updatedBy = users.find((u) => userId && u._id === userId);
          if (editExampleMode === exampleId) {
            return (
              <ExampleTextEditor
                example={props.row.example}
                onCancel={() => {
                  setEditExampleMode(null);
                }}
                onSave={handleExampleTextSave}
              />
            );
          }
          return canonicalTooltip(
            <div className="example-table-row">
              <UserUtteranceViewer
                value={props.value}
                onChange={handleEditExample}
                projectId=""
                disableEditing={!canEdit || canonical}
                showIntent={false}
              />
              <IconButton
                disabled={!canEdit || canonical}
                basic
                icon="edit"
                onClick={(e) => handleEditExampleClick(e, exampleId)}
                iconClass={canonical ? "disabled-delete" : undefined}
              />
              {updatedBy && (
              <Popup
                trigger={<div><IconButton
                  basic
                  disabled={true}
                  icon="question circle"
                /></div>}
                content={(
                  <Label
                      basic
                      content={(
                          <div>
                              Updated by {updatedBy.emails[0].address} <br/> <br/>
                              at {moment(updatedAt).fromNow()}
                          </div>
                      )}
                      style={{
                          borderColor: '#2c662d',
                          color: '#2c662d',
                      }}
                  />
              )}
              />)}
            </div>,
            canonical
          );
        },
        style: { overflow: "visible" },
      },
    ];

    if (isManager) {
      columns.push(
        {
          accessor: "_id",
          filterable: false,
          Cell: (props) => {
            const { isTestingData } = props.row.example;

            return (
              <Popup
                position="top center"
                trigger={
                  <div>
                    <IconButton
                      basic
                      icon="exchange"
                      size="small"
                      onClick={() => handleEditExample({ ...props.row.example, isTestingData: !isTestingData })}
                    />
                  </div>
                }
                inverted
                content={isTestingData ? "Move example to train set" : "Move example to test set"}
              />
            );
          },
          Header: "",
          width: 50,
        },
      )
    }

    if (canEdit) {
      columns.push(
        {
          accessor: "_id",
          filterable: false,
          Cell: (props) => {
            const {
              status: currentStatus, // 0: not validated, 1: validated, 2: confused
              metadata: { canonical = false } = {},
            } = props.row.example;
            let color = "grey";
            if (currentStatus === 2) {
              color = "yellow";
            } else if (currentStatus === 1) {
              color = "green";
            }
            return (
              <Popup
                position="top center"
                disabled={!canonical}
                trigger={
                  <div>
                    <IconButton
                      icon="check"
                      size="small"
                      active={currentStatus ? true : false}
                      disabled={canonical}
                      color={color}
                      onClick={() => {
                        let status = null;
                        if (clickTimeout !== null) {
                          if (
                            !currentStatus ||
                            currentStatus === 0 ||
                            currentStatus === 1
                          ) {
                            status = 2;
                            handleEditExample({ ...props.row.example, status });
                            clearTimeout(clickTimeout);
                            setClickTimeout(null);
                          }
                        } else {
                          setClickTimeout(
                            setTimeout(() => {
                              status =
                                !currentStatus ||
                                currentStatus === 0 ||
                                currentStatus === 2
                                  ? 1
                                  : 0;
                              handleEditExample({
                                ...props.row.example,
                                status,
                              });
                              clearTimeout(clickTimeout);
                              setClickTimeout(null);
                            }, 220)
                          );
                        }
                      }}
                    />
                  </div>
                }
                inverted
                content="Cannot validate a canonical example"
              />
            );
          },
          Header: "",
          width: 50,
        },
        {
          accessor: "_id",
          filterable: false,
          Cell: (props) => {
            const {
              metadata: { canonical = false } = {},
              deleted,
            } = props.row.example;
            let className = canonical ? "disabled-delete" : "";
            className += deleted ? "always-interactable" : "";
            return (
              <Popup
                position="top center"
                disabled={!canonical}
                trigger={
                  <div>
                    <IconButton
                      icon={deleted ? "redo" : "trash"}
                      basic
                      disabled={canonical}
                      onClick={() => {
                        setDeleteExample({
                          confirmOpen: true,
                          _id: props.value,
                        });
                      }}
                      data-cy="icon-trash"
                      className={className}
                    />
                  </div>
                }
                inverted
                content="Cannot delete a canonical example"
              />
            );
          },
          Header: "",
          width: 40,
        }
      );
    }

    return columns;
  };

  const canonicalTooltip = (jsx, canonical) => {
    if (!canonical) return jsx;
    return (
      <Popup
        trigger={<div>{jsx}</div>}
        inverted
        content="Cannot edit a canonical example"
      />
    );
  };

  const handleExampleTextSave = (example) => {
    if (
      // no text or text didn't change
      !example.text.trim() ||
      example.text ===
        (examples.find(({ _id }) => _id === example._id) || {}).text
    ) {
      return setEditExampleMode(null);
    }
    return Promise.resolve(handleEditExample(example)).then((res) => {
      if (!res) {
        Alert.error("Error occurred while updating example", {
          position: "top-right",
          timeout: "none",
        });
        return;
      }
      setEditExampleMode(null);
    });
  };

  const fetchData = (state) => {
    updatePagination(state.pageSize, state.page);
  };

  const handleEditExampleClick = (event, exampleId) => {
    if (editExampleMode === exampleId) return;
    setEditExampleMode(exampleId);
  };

  const renderDataTable = () => {
    const headerStyle = {
      textAlign: "left",
      fontWeight: 800,
      paddingBottom: "10px",
    };
    const columns = getColumns();
    const { confirmOpen: confirmOpen, _id: _id } = deleteExample;

    return (
      <Tab.Pane as="div" className={className}>
        <Confirm
          open={confirmOpen}
          header="Are you sure?"
          content="This cannot be undone!"
          onCancel={() => setDeleteExample({ confirmOpen: false, _id: "" })}
          onConfirm={() => {
            Promise.resolve(onDeleteExample([_id])).then((res) => {
              if (!res) {
                Alert.error("Error occurred while deleting example", {
                  position: "top-right",
                  timeout: "none",
                });
                return;
              }
              setDeleteExample({ confirmOpen: false, _id: "" });
            });
          }}
        />
        {!hideHeader && (
          <div
            className="side-by-side middle"
            style={{ paddingBottom: "12px" }}
          >
            <Grid>
              <Grid.Row>
                <Grid.Column>
                  <Filters
                    intents={intents}
                    entities={entities}
                    filter={filters}
                    statusOptions={[
                      { value: -1, text: "All data" },
                      { value: 0, text: "Not validated" },
                      { value: 1, text: "Validated" },
                      { value: 2, text: "Confused" },
                    ]}
                    usersOptions={usersOptions}
                    onChange={(newFilters) =>
                      updateFilters({ ...filters, ...newFilters })
                    }
                  />
                </Grid.Column>
              </Grid.Row>
            </Grid>
            {isAnnotator ? (
              <Popup
                trigger={
                  <Button.Group basic>
                    <Button
                      disabled={doing}
                      content={
                        <Dropdown
                          onClick={() =>
                            setTakingExamplesOpen(!takingExamplesOpen)
                          }
                          onClose={() => setTakingExamplesOpen(false)}
                          placeholder="Number of examples"
                          size="tiny"
                          open={takingExamplesOpen}
                          onChange={(_, { value }) => setNumberExamples(value)}
                          defaultValue={20}
                          floating
                          options={[
                            { text: "5", value: 5 },
                            { text: "10", value: 10 },
                            { text: "20", value: 20 },
                            { text: "25", value: 25 },
                            { text: "50", value: 50 },
                            { text: "100", value: 100 },
                          ]}
                        />
                      }
                      onClick={() => setTakingExamplesOpen(!takingExamplesOpen)}
                    />
                    <Button
                      disabled={doing}
                      content={<Icon name="arrow circle down" color="black" />}
                      onClick={() => getMoreExamples(numberExamples)}
                    />
                  </Button.Group>
                }
                content={
                  !doing
                    ? "Taking examples for annotation"
                    : "You must review all assigned examples before get new examples"
                }
                inverted={doing}
                position="top center"
              />
            ) : (
              <div>
                <Checkbox
                  style={{ marginRight: "10px", verticalAlign: 'middle' }}
                  onChange={() =>
                    updateFilters({
                      ...filters,
                      isTestingData: !filters.isTestingData,
                    })
                  }
                  slider
                  checked={filters.isTestingData}
                />
                <Popup
                  trigger={
                    <Icon
                      name="lab"
                      color={filters.isTestingData ? "black" : "grey"}
                    />
                  }
                  content={filters.isTestingData ? "Switch to train set" : "Switch to test set"}
                  position="top center"
                  inverted
                />
              </div>
            )}
          </div>
        )}
        {examples.length > 0 ? (
          <div className="glow-box extra-padding no-margin">
            <ReactTable
              manual
              pages={pages}
              onFetchData={fetchData}
              data={examples}
              columns={columns}
              minRows={1}
              style={{ overflow: "visible" }}
              getTbodyProps={() => ({
                style: {
                  overflow: "visible",
                },
              })}
              getTableProps={() => ({
                style: {
                  overflow: "visible",
                },
              })}
              getTdProps={() => ({
                style: {
                  display: "flex",
                  flexDirection: "column",
                  justifyContent: "center",
                },
              })}
              getTheadThProps={() => ({
                style: {
                  borderRight: "none",
                  ...headerStyle,
                },
              })}
              getTrProps={(__, rowInfo = {}) => {
                if (!conditionalRowFormatter) return {};
                return conditionalRowFormatter(rowInfo.original);
              }}
            />
          </div>
        ) : (
          <Message
            success
            icon="check"
            header="No examples"
            content={
              isAnnotator
                ? "Click on the download button(arrow on the right corner of the screen) to start annotate and enjoy your data!"
                : "No examples was found for the given criteria."
            }
          />
        )}
      </Tab.Pane>
    );
  };
  return renderDataTable();
}

NluDataTable.propTypes = {
  examples: PropTypes.array.isRequired,
  onEditExample: PropTypes.func.isRequired,
  onDeleteExample: PropTypes.func.isRequired,
  hideHeader: PropTypes.bool,
  onSwitchCanonical: PropTypes.func.isRequired,
  getMoreExamples: PropTypes.func.isRequired,
  conditionalRowFormatter: PropTypes.func,
  className: PropTypes.string,
  updateFilters: PropTypes.func,
  filters: PropTypes.object,
  pages: PropTypes.number,
  updatePagination: PropTypes.func.isRequired,
  canEdit: PropTypes.bool.isRequired,
  isManager: PropTypes.bool.isRequired,
  doing: PropTypes.bool,
  usersOptions: PropTypes.array,
};

NluDataTable.defaultProps = {
  hideHeader: false,
  className: "",
  updateFilters: () => {},
  updatePagination: () => {},
  filters: null,
  pages: 1,
};

const NluDataTableContainer = (props) => {
  const { isManager} = props;
  let usersOptions = [];
  const users = Meteor.users.find({}).fetch();
  if (isManager) {
    let tmpUsers = [];
    const rolesAssigned = Meteor.roleAssignment.find({}).fetch();
    users.forEach((u) => {
      const assigned = rolesAssigned.find((r) => r.user._id === u._id);
      if (assigned && assigned.role._id === "annotator") {
        tmpUsers.push({ ...u });
      }
    });
  
    usersOptions = tmpUsers.map((u) => ({
      text: u.emails[0].address,
      value: u._id,
    }));
    usersOptions.unshift({ text: "All annotator", value: "" });
  }
  const componentProps = {
    ...props,
    users,
    usersOptions,
  };

  return (<NluDataTable {...componentProps} />);
};

export default NluDataTableContainer;
