import gql from "graphql-tag";

const entityFields = gql`
  fragment EntityFields on Entity {
    entity
    value
    group
    role
    start
    end
  }
`;

export const GET_INTENT_STATISTICS = gql`
  query getIntentStatistics($projectId: String!, $language: String!) {
    getIntentStatistics(projectId: $projectId, language: $language) {
      intent
      example
      counts {
        type
        count
      }
    }
  }
`;

export const GET_ANNOTATOR_STATISTICS = gql`
  query getAnnotatorStatistics($projectId: String!, $language: String!) {
    getAnnotatorStatistics(projectId: $projectId, language: $language) {
      annotator
      status {
        value
        count
      }
    }
  }
`;

export const GET_EXAMPLE_COUNT = gql`
  query examples($projectId: String!, $language: String!, $userIdFilter: String, $status: Int = -1, $isTestingData: Boolean = false) {
    examples(projectId: $projectId, language: $language, userIdFilter:$userIdFilter, status:$status, isTestingData: $isTestingData) {
      pageInfo {
        totalLength
      }
    }
  }
`;

export const GET_EXAMPLES = gql`
  query examples(
    $projectId: String!
    $language: String!
    $intents: [String]
    $entities: [Any]
    $onlyCanonicals: Boolean
    $text: [String]
    $userIdFilter: String
    $status: Int = -1
    $isTestingData: Boolean
    $order: order
    $sortKey: String
    $pageSize: Int
    $pageIndex: Int
    $cursor: String
    $matchEntityName: Boolean = false
  ) {
    examples(
      projectId: $projectId
      language: $language
      intents: $intents
      entities: $entities
      onlyCanonicals: $onlyCanonicals
      text: $text
      userIdFilter: $userIdFilter
      status: $status
      isTestingData: $isTestingData
      order: $order
      sortKey: $sortKey
      pageSize: $pageSize
      pageIndex: $pageIndex
      cursor: $cursor
      matchEntityName: $matchEntityName
    ) {
      examples {
        _id
        projectId
        text
        intent
        entities {
          ...EntityFields
        }
        status
        isTestingData
        metadata
        updatedAt
        updatedBy
      }
      pageInfo {
        endCursor
        hasNextPage
        doing
        totalLength
      }
    }
  }
  ${entityFields}
`;

export const LIST_INTENTS_AND_ENTITIES = gql`
  query listIntentsAndEntities($projectId: String!, $language: String!) {
    listIntentsAndEntities(projectId: $projectId, language: $language) {
      intents
      entities
    }
  }
`;

export const INTENTS_OR_ENTITIES_CHANGED = gql`
  subscription intentsOrEntitiesChanged(
    $projectId: String!
    $language: String!
  ) {
    intentsOrEntitiesChanged(projectId: $projectId, language: $language) {
      changed
    }
  }
`;

export const EXAMPLE_CHANGED = gql`
  subscription exampleChanged(
    $projectId: String!
    $language: String!
  ) {
    exampleChanged(projectId: $projectId, language: $language) {
      changed
    }
  }
`;

export const INSERT_EXAMPLES = gql`
  mutation insertExamples(
    $projectId: String!
    $language: String!
    $examples: [ExampleInput]!
    $options: Any = {}
  ) {
    insertExamples(
      projectId: $projectId
      language: $language
      examples: $examples
      options: $options
    ) {
      _id
      projectId
      text
      intent
      entities {
        ...EntityFields
      }
      status
      metadata
    }
  }
  ${entityFields}
`;

export const DELETE_EXAMPLES = gql`
  mutation deleteExamples($ids: [String]!, $projectId: String!, $language: String!) {
    deleteExamples(ids: $ids, projectId: $projectId, language: $language)
  }
`;

export const SWITCH_CANONICAL = gql`
  mutation switchCanonical(
    $projectId: String
    $language: String
    $example: ExampleInput!
  ) {
    switchCanonical(
      projectId: $projectId
      language: $language
      example: $example
    ) {
      _id
      projectId
      text
      intent
      entities {
        ...EntityFields
      }
      status
      metadata
    }
  }
  ${entityFields}
`;

export const UPDATE_EXAMPLES = gql`
  mutation updateExamples(
    $projectId: String!
    $language: String!
    $examples: [ExampleInput]!
  ) {
    updateExamples(
      projectId: $projectId
      language: $language
      examples: $examples
    ) {
      _id
      projectId
      text
      intent
      entities {
        ...EntityFields
      }
      status
      metadata
    }
  }
  ${entityFields}
`;

export const ASSIGN_EXAMPLES = gql`
  mutation assignExamples(
    $projectId: String!
    $language: String!
    $intents: [String]
    $entities: [Any]
    $numberExamples: Int = 20
  ) {
    assignExamples(
      projectId: $projectId
      language: $language
      intents: $intents
      entities: $entities
      numberExamples: $numberExamples
    ) {
      _id
      projectId
      text
      intent
      entities {
        ...EntityFields
      }
      status
      metadata
    }
  }
  ${entityFields}
`;
