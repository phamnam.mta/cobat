import React, { useState } from "react";
import PropTypes from "prop-types";
import { Comment, Popup } from "semantic-ui-react";

import IntentLabel from "../nlu/common/IntentLabel";

export default function UserUtteredEventEditor({
  event,
  author,
  handleDelete,
  handleInsert,
  handleIntentChange,
}) {
  const [active, setActive] = useState("");

  tooltipWrapper = (trigger, tooltip) => (
    <Popup size="mini" inverted content={tooltip} trigger={trigger} />
  );

  return (
    <Comment.Content>
      <Comment.Author as="a">{author || "User"}</Comment.Author>
      <Comment.Metadata>
        <span>{event.timestamp.format("ddd, MMM Do, h:mm:ss a")}</span>
      </Comment.Metadata>
      <Comment.Text className="conversation-user-utterance">
        <span style={{ marginRight: "10px" }}>{event.example.text}</span>
        <IntentLabel
          value={event.example.intent}
          allowEditing={true}
          allowAdditions
          onChange={(intent) => handleIntentChange(event._id, intent)}
        />
        <Comment.Metadata className="conversation-utterance-confidence">
          (
          {event.confidence === 1
            ? "100%"
            : `${(event.confidence * 100).toFixed(2)}%`}
          )
        </Comment.Metadata>
      </Comment.Text>
      <Comment.Actions>
        {this.tooltipWrapper(
          <a
            className={active}
            onClick={() => {
              setActive("not-active");
              handleInsert(event._id);
            }}
          >
            Add message
          </a>,
          "Add message to training data"
        )}
        {this.tooltipWrapper(
          <a
            onClick={() => {
              handleDelete(event._id);
            }}
          >
            Delete
          </a>,
          "Delete the message and re-compose the conversation at this turn"
        )}
      </Comment.Actions>
    </Comment.Content>
  );
}

UserUtteredEventEditor.propTypes = {
  event: PropTypes.object.isRequired,
  author: PropTypes.string,
  handleDelete: PropTypes.func.isRequired,
  handleInsert: PropTypes.func.isRequired,
  handleIntentChange: PropTypes.func.isRequired,
};

UserUtteredEventEditor.defaultProps = {
  author: null,
};
