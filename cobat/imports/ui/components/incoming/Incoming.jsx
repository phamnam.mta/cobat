import React, { useState, useContext, useEffect } from 'react';
import PropTypes from 'prop-types';

import { Container, Menu, Label } from 'semantic-ui-react';
import { browserHistory } from 'react-router';

import LanguageDropdown from '../common/LanguageDropdown';
import { PageMenu } from '../utils/Utils';
import Activity from '../nlu/activity/Activity';
import { useCountActivity } from '../nlu/activity/hooks'
import ActivityInsertions from '../nlu/activity/ActivityInsertions';
import ConversationBrowser from '../conversations/ConversationsBrowser';
import { updateIncomingPath } from './incoming.utils';
import { ProjectContext } from '../../layouts/context';

export default function Incoming(props) {
    const { router } = props;
    const [activeTab, setActiveTab] = useState(router.params.tab || 'newutterances');
    const {
        project: { _id: projectId },
        language: workingLanguage,
    } = useContext(ProjectContext);
    const { totalLength } = useCountActivity({ projectId, language: workingLanguage });

    const linkToEvaluation = () => {
        router.push({
            pathname: `/project/${projectId}/nlu/model/${workingLanguage}`,
            state: { isActivityLinkRender: true },
        });
    };

    const handleTabClick = (itemKey) => {
        setActiveTab(itemKey);
        const url = updateIncomingPath({ ...router.params, tab: itemKey });
        if (router.params.tab === url) return;
        browserHistory.push({ pathname: url });
    };

    const renderPageContent = () => {
        switch (activeTab) {
        case 'newutterances':
            return <Activity linkRender={linkToEvaluation}/>;
        case 'populate':
            return <ActivityInsertions />;
        case 'conversations':
            return <ConversationBrowser />;
        default:
            return <></>;
        }
    };

    const renderTabs = () => (
        [
            { value: 'populate', text: 'Populate' },
            { value: 'conversations', text: 'Conversations' },
        ].map(({ value, text }) => (
            <Menu.Item
                content={text}
                key={value}
                data-cy={value}
                active={value === activeTab}
                onClick={() => handleTabClick(value)}
            />
        ))
    );

    return (
        <>
            <PageMenu>
                <Menu.Item>
                    {['conversations', 'forms'].includes(activeTab)
                        ? null
                        : <LanguageDropdown />
                    }
                </Menu.Item>
                <Menu.Item>
                    <Menu.Item
                        content={(<>New Utterances<Label circular pointing='left' color='red'>{totalLength}</Label></>)}
                        key='newutterances'
                        active={'newutterances' === activeTab}
                        onClick={() => handleTabClick('newutterances')}
                    />
                    {renderTabs()}
                </Menu.Item>
            </PageMenu>
            <div>
                <Container>{renderPageContent()}</Container>
            </div>
        </>
    );
}

Incoming.propTypes = {
    router: PropTypes.object,
};

Incoming.defaultProps = {
    router: {},
};
