import React, { useContext } from 'react';
import PropTypes from 'prop-types';
import {
    Icon, Loader, Menu, Label, Popup,
} from 'semantic-ui-react';
import moment from 'moment';
import { isTraining } from '../../../api/nlu_model/nlu_model.utils';
import TrainButton from './TrainButton';
import { ProjectContext } from '../../layouts/context';
import { can } from '../../../lib/scopes';


export function Loading({ loading, children }) {
    return !loading ? children : <Loader active inline='centered' />;
}


export function tooltipWrapper(trigger, tooltip) {
    return (
        <Popup size='mini' inverted content={tooltip} trigger={trigger} />
    );
}

Loading.propTypes = {
    loading: PropTypes.bool.isRequired,
};


export function PageMenu(props) {
    const {
        title, icon, children, className, headerDataCy, mode,
    } = props;
    const {
        project,
        project: { _id: projectId, training: { endTime, status } = {} } = {},
        instance,
    } = useContext(ProjectContext);
    const canModelEdit = can('nlu-model:w', projectId);
    return (
        <Menu borderless className={`top-menu ${className}`}>
            <Menu.Item>
                <Menu.Header as='h3'>
                    {icon && <Icon name={icon} {...(headerDataCy ? { 'data-cy': headerDataCy } : {})} />}
                    {` ${title || ''}`}
                </Menu.Header>
            </Menu.Item>
            {children}
            {canModelEdit && mode && (
                <Menu.Menu position="right">
                    <Menu.Item position='right'>
                        {!isTraining(project) && status === 'success' && (
                            <Popup
                                trigger={(
                                    <Icon
                                        size='small'
                                        name='check'
                                        fitted
                                        circular
                                        style={{ color: '#2c662d' }}
                                    />
                                )}
                                content={(
                                    <Label
                                        basic
                                        content={(
                                            <div>
                                                {`Trained ${moment(endTime).fromNow()}`}
                                            </div>
                                        )}
                                        style={{
                                            borderColor: '#2c662d',
                                            color: '#2c662d',
                                        }}
                                    />
                                )}
                            />
                        )}
                        {!isTraining(project) && status === 'failure' && (
                            <Popup
                                trigger={(
                                    <Icon
                                        size='small'
                                        name='warning'
                                        color='red'
                                        fitted
                                        circular
                                    />
                                )}
                                content={(
                                    <Label
                                        basic
                                        color='red'
                                        content={(
                                            <div>
                                                {`Training failed ${moment(
                                                    endTime,
                                                ).fromNow()}`}
                                            </div>
                                        )}
                                    />
                                )}
                            />
                        )}
                    </Menu.Item>
                    <Menu.Item position='right'>
                        <TrainButton
                            project={project}
                            instance={instance}
                            projectId={projectId}
                            mode={mode}
                        />
                    </Menu.Item>
                </Menu.Menu>
            )}
        </Menu>
    );
}

PageMenu.defaultProps = {
    children: null,
    className: '',
    headerDataCy: null,
    mode: '',
    icon: null,
    title: '',
};

PageMenu.propTypes = {
    title: PropTypes.string,
    icon: PropTypes.string,
    children: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
    className: PropTypes.string,
    headerDataCy: PropTypes.string,
    mode: PropTypes.string,
};
