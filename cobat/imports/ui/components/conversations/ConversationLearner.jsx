import React, { useState, useEffect, useRef } from "react";
import Alert from "react-s-alert";
import { Meteor } from "meteor/meteor";
import uuidv4 from "uuid/v4";
import PropTypes from "prop-types";
import {
  Icon,
  Menu,
  Segment,
  Placeholder,
  Ref,
  Popup,
} from "semantic-ui-react";
import { useQuery } from "@apollo/react-hooks";
import { connect } from "react-redux";
import AceEditor from "react-ace";
import { GET_CONVERSATION } from "./queries";
import ConversationDialogueEditor from "./ConversationDialogueEditor";

function ConversationLearner(props) {
  const {
    tracker,
    ready,
    conversationId,
    onSave,
    projectId,
    workingLanguage,
    onClickBotResponse,
  } = props;

  const [active, setActive] = useState("Learn");
  const [story, setStory] = useState("");
  const scrollView = useRef(null);

  useEffect(() => {
    setActive("Learn");
  }, [tracker]);

  useEffect(() => {
    if (scrollView.current) {
      scrollView.current.scrollTop = scrollView.current.scrollHeight;
    }
  }, [tracker]);

  function handleSave() {
    if (!story) {
      Meteor.call("rasa.story", projectId, conversationId, (err, res) => {
        if (err) {
          Alert.error(`Error: ${err.reason || err.error || err.message}`, {
            position: "top-right",
            timeout: "none",
          });
          return;
        }
        onSave(tracker._id, res);
      });
    } else {
      onSave(tracker._id, story);
    }
  }

  function handleItemClick(event, item) {
    if (item.name == "Story") {
      Meteor.call("rasa.story", projectId, conversationId, (err, res) => {
        if (err) {
          Alert.error(`Error: ${err.reason || err.error || err.message}`, {
            position: "top-right",
            timeout: "none",
          });
          return;
        }
        setStory(res);
      });
    }
    setActive(item.name);
  }
  function tooltipWrapper(trigger, tooltip) {
    return <Popup size="mini" inverted content={tooltip} trigger={trigger} />;
  }

  function renderSegment() {
    const style = {
      height: "75vh",
      maxHeight: "75vh",
      overflowY: "scroll",
      borderBottom: 0,
    };

    if (!ready) {
      return (
        <Segment style={style} attached>
          <Placeholder>
            <Placeholder.Header image>
              <Placeholder.Line />
              <Placeholder.Line />
            </Placeholder.Header>
            <Placeholder.Paragraph>
              <Placeholder.Line />
              <Placeholder.Line />
              <Placeholder.Line />
              <Placeholder.Line />
            </Placeholder.Paragraph>
          </Placeholder>
          <Placeholder>
            <Placeholder.Header image>
              <Placeholder.Line />
              <Placeholder.Line />
            </Placeholder.Header>
            <Placeholder.Paragraph>
              <Placeholder.Line />
              <Placeholder.Line />
              <Placeholder.Line />
              <Placeholder.Line />
            </Placeholder.Paragraph>
          </Placeholder>
        </Segment>
      );
    }

    return (
      <Ref innerRef={scrollView}>
        <Segment style={style} attached>
          {active === "Learn" && (
            <ConversationDialogueEditor
              conversation={tracker}
              onClickBotResponse={onClickBotResponse}
              mode="learn"
              projectId={projectId}
              workingLanguage={workingLanguage}
              conversationId={conversationId}
            />
          )}
          {active === "Story" && (
            <AceEditor
              theme="github"
              name="story"
              mode="text"
              maxLines={100}
              fontSize={14}
              readOnly={true}
              value={story}
            />
          )}
        </Segment>
      </Ref>
    );
  }

  return (
    <div>
      <Menu compact attached="top" style={{ borderBottom: 0 }}>
        <Menu.Item header>
          <span>{conversationId}</span>
        </Menu.Item>
        <Menu.Menu position="right">
          {tooltipWrapper(
            <Menu.Item
              name="Learn"
              disabled={!ready}
              active={ready && active === "Learn"}
              onClick={handleItemClick}
            >
              <Icon name="comments" />
            </Menu.Item>,
            "Conversation viewer"
          )}
          {tooltipWrapper(
            <Menu.Item
              name="Story"
              disabled={!ready}
              active={ready && active === "Story"}
              onClick={handleItemClick}
            >
              <Icon name="code" />
            </Menu.Item>,
            "Markdown viewer"
          )}
          {tooltipWrapper(
            <Menu.Item name="save" disabled={!ready} onClick={handleSave}>
              <Icon name="save" />
            </Menu.Item>,
            "Save conversation to story"
          )}
        </Menu.Menu>
      </Menu>
      {renderSegment()}
    </div>
  );
}

ConversationLearner.defaultProps = {
  tracker: null,
};

ConversationLearner.propTypes = {
  tracker: PropTypes.object,
  onSave: PropTypes.func.isRequired,
  onClickBotResponse: PropTypes.func.isRequired,
  conversationId: PropTypes.string,
  ready: PropTypes.bool.isRequired,
  projectId: PropTypes.string.isRequired,
};

const ConversationLearnerContainer = (props) => {
  const {
    conversationId,
    projectId,
    workingLanguage,
    onSave,
    onClickBotResponse,
  } = props;

  const tracker = useRef(null);

  const { loading, error, data } = useQuery(GET_CONVERSATION, {
    variables: { projectId, conversationId },
    pollInterval: 1000,
  });

  const newTracker = !loading && !error && data ? data.conversation : null;
  if (
    (newTracker &&
      ((tracker.current ? tracker.current.tracker.events : []).length !==
        newTracker.tracker.events.length ||
        (tracker.current || {})._id !== newTracker._id)) ||
    (newTracker && tracker.current !== newTracker)
  ) {
    const events = newTracker.tracker.events.map((obj) => ({
      ...obj,
      _id: uuidv4(),
    }));
    const trackerExtend = { ...newTracker.tracker, events };
    Object.assign(newTracker, { tracker: trackerExtend });
    tracker.current = newTracker;
  }

  const componentProps = {
    ready: !!tracker.current,
    conversationId,
    onSave,
    onClickBotResponse,
    tracker: tracker.current,
    projectId,
    workingLanguage,
  };

  return <ConversationLearner {...componentProps} />;
};

const mapStateToProps = (state) => ({
  projectId: state.settings.get("projectId"),
  workingLanguage: state.settings.get("workingLanguage"),
});

export default connect(mapStateToProps)(ConversationLearnerContainer);
