import React, { useMemo, useContext, useRef, useEffect } from "react";
import { Meteor } from "meteor/meteor";
import PropTypes from "prop-types";
import ReactJson from "react-json-view";
import Alert from "react-s-alert";
import { Comment, Message, Popup } from "semantic-ui-react";
import { generateTurns } from "./utils";
import { ProjectContext } from "../../layouts/context";
import UserUtteredEventEditor from "../example_editor/UserUtteredEventEditor";

function BotResponse({ type, text, data, _id, onClickResponse }) {
  const wrapperRef = useRef(null);
  if (!text && !data && !_id) {
    return null;
  }
  function handleClickOutside(event) {
    if (wrapperRef.current) {
      const actionTable = document.getElementById("list-bot-actions");
      if (actionTable && actionTable.contains(event.target)) return;
      if (wrapperRef.current.contains(event.target)) {
        wrapperRef.current.style.background = "#767676";
      } else {
        wrapperRef.current.style.background = "#e9eef3";
        onClickResponse();
      }
    }
  }

  useEffect(() => {
    document.addEventListener("mousedown", handleClickOutside);
    return () => {
      document.removeEventListener("mousedown", handleClickOutside); // will unmount
    };
  }, [wrapperRef.current]);
  // remove empty attributes
  if (data)
    Object.keys(data).forEach((_id) => data[_id] == null && delete data[_id]);

  const dataEmpty = !data || !Object.keys(data).length;

  handleItemClick = (_id) => {
    onClickResponse(_id);
  };

  tooltipWrapper = (trigger, tooltip) => {
    return <Popup size="mini" inverted content={tooltip} trigger={trigger} />;
  };

  return this.tooltipWrapper(
    <div
      className="bot-response-message"
      key={_id}
      onClick={() => this.handleItemClick(_id)}
      ref={wrapperRef}
    >
      {text && <p className="bot-response-text">{text}</p>}
      {!dataEmpty && (
        <ReactJson
          className="bot-response-json"
          src={data}
          collapsed
          name={type}
        />
      )}
    </div>,
    "Click to change Bot response"
  );
}

BotResponse.propTypes = {
  type: PropTypes.string.isRequired,
  text: PropTypes.string,
  data: PropTypes.object,
  _id: PropTypes.string,
  onClickResponse: PropTypes.func,
};

BotResponse.defaultProps = {
  text: "",
  data: null,
  _id: "bot-response",
};

function Turn({
  userSays,
  userId,
  botResponses,
  key,
  onClickResponse,
  handleDeleteUserInput,
  handleInsertUserInput,
  handleIntentChange,
}) {
  if (!userSays && botResponses.length === 0) {
    return null;
  }

  return (
    <Comment key={key}>
      {userSays &&
        userSays.example.intent !== "session_start" && [
          <Comment.Avatar src="/images/avatars/matt.jpg" />,
          <UserUtteredEventEditor
            event={userSays}
            author={userId}
            handleDelete={handleDeleteUserInput}
            handleInsert={handleInsertUserInput}
            handleIntentChange={handleIntentChange}
          />,
        ]}
      {botResponses.length > 0 && (
        <Comment.Group>
          <Comment>
            <Comment.Avatar src="/images/avatars/mrbot.png" />
            <Comment.Content>
              <Comment.Author as="a">Bot</Comment.Author>
              <Comment.Metadata></Comment.Metadata>
              <Comment.Text>
                {botResponses.map((response, index) => (
                  <BotResponse
                    {...response}
                    onClickResponse={onClickResponse}
                    key={`bot-response-${index}`}
                  />
                ))}
              </Comment.Text>
              <Comment.Actions></Comment.Actions>
            </Comment.Content>
          </Comment>
        </Comment.Group>
      )}
    </Comment>
  );
}

Turn.propTypes = {
  userSays: PropTypes.object,
  userId: PropTypes.string,
  botResponses: PropTypes.arrayOf(PropTypes.object).isRequired,
  key: PropTypes.string,
  onClickResponse: PropTypes.func,
  handleDeleteUserInput: PropTypes.func,
  handleInsertUserInput: PropTypes.func,
  handleIntentChange: PropTypes.func,
};

Turn.defaultProps = {
  userSays: null,
  userId: null,
  key: "dialogue-turn",
};

function ConversationDialogueEditor({
  conversation: { tracker, userId },
  mode,
  onClickBotResponse,
  projectId,
  language,
  conversationId,
}) {
  const { timezoneOffset, addUtterancesToTrainingData } = useContext(ProjectContext);
  const turns = useMemo(
    () => generateTurns(tracker, mode === "debug", timezoneOffset),
    [tracker]
  );

  function onClickResponse(_id = "") {
    if (!_id) {
      onClickBotResponse([]);
    } else {
      const idx = tracker.events.findIndex((t) => t._id === _id);
      if (idx - 1 > 0) {
        const events = tracker.events.slice(0, idx - 1);
        onClickBotResponse(events);
      }
    }
  }

  function deleteUserInput(_id) {
    const idx = tracker.events.findIndex((t) => t._id === _id);
    const events = tracker.events.slice(0, idx).map(({ _id, ...item }) => item);
    Meteor.call(
      "rasa.tracker.events",
      projectId,
      conversationId,
      events,
      (err, res) => {
        if (err) {
          Alert.error(`Error: ${err.reason || err.error || err.message}`, {
            position: "top-right",
            timeout: "none",
          });
          return;
        }
      }
    );
  }

  function intentChange(_id, newIntent) {
    const idx = tracker.events.findIndex((t) => t._id === _id);
    const event = tracker.events.find((x) => x._id === _id);
    const intent = event.parse_data.intent_ranking.find(
      (x) => x.name === newIntent
    );

    const parse_data = { ...event.parse_data, intent };
    const newEvent = { ...event, parse_data };
    const events = [...tracker.events.slice(0, idx), { ...newEvent }].map(
      ({ _id, ...item }) => item
    );

    Meteor.call(
      "rasa.tracker.events",
      projectId,
      conversationId,
      events,
      (err, res) => {
        if (err) {
          Alert.error(`Error: ${err.reason || err.error || err.message}`, {
            position: "top-right",
            timeout: "none",
          });
          return;
        }
      }
    );
  }

  function insertUserInput(_id) {
    const event = tracker.events.find((x) => x._id === _id);
    const utterances = [
      {
        entities: event.parse_data.entities,
        intent: event.parse_data.intent.name,
        text: event.text,
        status: 1,
        _id: event._id,
      },
    ];

    addUtterancesToTrainingData(utterances, (err) => {
      if (err) {
        Alert.error(`Error: ${err.reason || err.error || err.message}`, {
          position: "top-right",
          timeout: "none",
        });
        return;
      }
      Alert.success("Saved message to training data", {
        position: "top-right",
        timeout: 1000,
      });
    });
  };

  return (
    <Comment.Group>
      {turns.length > 0 ? (
        turns.map(({ userSays, botResponses }, index) => (
          <Turn
            userSays={userSays}
            userId={userId}
            botResponses={botResponses}
            onClickResponse={onClickResponse}
            handleDeleteUserInput={deleteUserInput}
            handleInsertUserInput={insertUserInput}
            handleIntentChange={intentChange}
            key={`dialogue-turn-${index}`}
          />
        ))
      ) : (
        <Message
          info
          icon="warning"
          header="No events to show"
          content={(() => {
            if (mode !== "debug") {
              return "check debug mode for non-dialogue events.";
            }

            return "check JSON mode to view the full tracker object.";
          })()}
        />
      )}
    </Comment.Group>
  );
}

ConversationDialogueEditor.propTypes = {
  conversation: PropTypes.object.isRequired,
  mode: PropTypes.string,
  onClickBotResponse: PropTypes.func.isRequired,
  projectId: PropTypes.string.isRequired,
  conversationId: PropTypes.string.isRequired,
  language: PropTypes.string.isRequired,
};

ConversationDialogueEditor.defaultProps = {
  mode: "text",
  projectId: "",
  conversationId: "",
  language: "",
};

export default ConversationDialogueEditorContainer = (props) => {
  const { workingLanguage: language } = props;

  const componentProps = {
    ...props,
    language,
  };

  return <ConversationDialogueEditor {...componentProps} />;
};
