/* eslint-disable camelcase */

export const updateInteractivePath = (params, exitBefore) => {
  const { project_id, page, selected_id } = params;
  let path = "/project";
  if (!project_id) {
    return path;
  }
  path = `${path}/${project_id}/interactive/${page}`;

  if (!selected_id || exitBefore === "selected_id") {
    return path;
  }
  path = `${path}/${selected_id}`;
  return path;
};
