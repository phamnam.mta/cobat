/* eslint-disable camelcase */
import React, { useState, useEffect, useRef } from "react";
import PropTypes from "prop-types";
import requiredIf from "react-required-if";
import { useQuery, useMutation } from "@apollo/react-hooks";
import Alert from "react-s-alert";
import { browserHistory, withRouter } from "react-router";
import { Meteor } from "meteor/meteor";
import {
  Grid,
  Menu,
  Pagination,
  Message,
  Form,
  Input,
  Icon,
  Segment,
  Popup,
} from "semantic-ui-react";
import { connect } from "react-redux";
import { GET_CONVERSATIONS } from "./queries";
import { DELETE_CONV } from "./mutations";
import "react-select/dist/react-select.css";
import ConversationLearner from "../conversations/ConversationLearner";
import LanguageDropdown from "../common/LanguageDropdown";
import { PageMenu } from "../utils/Utils";
import ActionCandidates from "./ActionCandidates";
import { Loading } from "../utils/Utils";
import { updateInteractivePath } from "./interactivePath.utils";
import { PREFIX_CONV, SESSION_START } from "../constants.json";

function InteractiveLearning(props) {
  const {
    page,
    pages,
    trackers,
    activeConversationId,
    refetch,
    router,
  } = props;

  const [confidences, setConfidences] = useState([]);
  const [conversationName, setConversationName] = useState("");
  const [userInput, setUserInput] = useState("");
  const [active, setActive] = useState(false);
  const [events, setEvents] = useState([]);
  const inputEl = useRef(null);

  const [deleteConv, { data }] = useMutation(DELETE_CONV);

  useEffect(() => {
    if (data && !data.delete.success) {
      Alert.warning("Something went wrong, the conversation was not deleted", {
        position: "top-right",
        timeout: 5000,
      });
    }
  }, [data]);

  useEffect(() => {
    confidences.length > 0 ? setActive(true) : setActive(false);
  }, [confidences]);

  useEffect(() => {
    if (trackers.length > 0 && !!inputEl.current && !active) {
      inputEl.current.focus();
    }
  }, [trackers, activeConversationId]);

  const goToConversation = (newPage, conversationId, replace = false) => {
    // let url = `/project/${projectId}/incoming/${modelId}/conversations/${page || 1}`;
    const url = updateInteractivePath({
      ...router.params,
      page: newPage || 1,
      selected_id: conversationId,
    });
    if (replace) return browserHistory.replace({ pathname: url });
    return browserHistory.push({ pathname: url });
  };

  function handleItemClick(event, { name }) {
    if (!active) {
      goToConversation(page, name);
    }
  }

  function handleSubmitMessage() {
    Meteor.call(
      "rasa.messages",
      router.params.project_id,
      activeConversationId,
      userInput,
      (err, res) => {
        if (err) {
          Alert.error(`Error: ${err.reason || err.error || err.message}`, {
            position: "top-right",
            timeout: "none",
          });
          return;
        }
        const { scores } = res;
        setConfidences(scores);
        setUserInput("");
      }
    );
  }

  function handleSubmitName() {
    const convId = `${PREFIX_CONV}${conversationName}`;
    if (trackers.some((c) => c._id === convId)) {
      Alert.warning(
        "The conversation name must be unique, the conversation was not inserted",
        {
          position: "top-right",
          timeout: 5000,
        }
      );
    } else if (!conversationName.match(/^[a-z0-9]+$/i)) {
      Alert.warning(
        "The conversation name can only contain alphanumeric characters, the conversation was not inserted",
        {
          position: "top-right",
          timeout: 5000,
        }
      );
    } else {
      Meteor.call(
        "rasa.messages",
        router.params.project_id,
        convId,
        SESSION_START,
        (err, res) => {
          if (err) {
            Alert.error(`Error: ${err.reason || err.error || err.message}`, {
              position: "top-right",
              timeout: "none",
            });
            return;
          }
          refetch();
          goToConversation(page, convId, true);
          setConversationName("");
        }
      );
    }
  }

  function pageChange(newPage) {
    const url = updateInteractivePath(
      { ...router.params, page: newPage || 1 },
      "selected_id"
    );
    return browserHistory.push({ pathname: url });
  }

  function renderMenuItems() {
    const items = trackers.map((t, index) => (
      <Menu.Item
        key={index.toString(10)}
        name={t._id}
        active={activeConversationId === t._id}
        onClick={handleItemClick}
      >
        <Grid>
          <Grid.Column width={14}>
            <span style={{ fontSize: "12px" }}>{t._id}</span>
          </Grid.Column>
          <Grid.Column width={2}>
            <Icon
              position="right"
              name="trash"
              size="small"
              color="grey"
              onClick={(e) => {
                e.preventDefault();
                e.stopPropagation();
                deleteConversation(t._id);
              }}
            />
          </Grid.Column>
        </Grid>
      </Menu.Item>
    ));
    return items;
  }

  function runAction(action) {
    Meteor.call(
      "rasa.execute",
      router.params.project_id,
      activeConversationId,
      action,
      (err, res) => {
        if (err) {
          Alert.error(`Error: ${err.reason || err.error || err.message}`, {
            position: "top-right",
            timeout: "none",
          });
          return;
        }
        if (Object.keys(res).length === 0 && res.constructor === Object) {
          setConfidences([]);
        } else {
          const { scores } = res;
          setConfidences(scores);
        }
      }
    );
  }

  function replaceAndRunAction(action) {
    const e = events.map(({ _id, ...item }) => item);
    Meteor.call(
      "rasa.tracker.events",
      router.params.project_id,
      activeConversationId,
      e,
      (err, res) => {
        if (err) {
          Alert.error(`Error: ${err.reason || err.error || err.message}`, {
            position: "top-right",
            timeout: "none",
          });
          return;
        }
        runAction(action);
        setEvents([]);
      }
    );
  }

  function deleteConversation(conversationId) {
    if (conversationId === activeConversationId) {
      const index = trackers.map((t) => t._id).indexOf(conversationId);
      // deleted convo is not the last of the current page
      if (index < trackers.length - 1) {
        goToConversation(page, trackers[index + 1]._id, true);
        // or deleted convo is the last but there is a next page
      } else if (index === trackers.length - 1 && trackers.length > 1) {
        goToConversation(page, trackers[index - 1]._id, true);
        // deleted convo is the last but not the only one and there is no next page
      } else if (index === 0 && trackers.length === 1) {
        goToConversation(Math.max(page - 1, 1), undefined, true);
      } else {
        goToConversation(Math.min(page - 1, 1), undefined, true);
      }
    }
    deleteConv({ variables: { id: conversationId } });
    refetch();
    setConfidences([]);
  }

  const handleImportStoryGroups = (stories) => {
    if (!stories.length) return;
    const callback = (err, res) => {
      if (err) {
        Alert.error(`Error: ${err.reason || err.error || err.message}`, {
          position: "top-right",
          timeout: "none",
        });
        return;
      }
    };
    const d = Date.now();
    const today = new Date(d);
    const name = `${activeConversationId}-${today.toLocaleDateString()}`;
    const projectId = router.params.project_id;
    const _id = `${PREFIX_CONV}${today.toLocaleDateString()}`;

    Meteor.call("storyGroups.insert", { _id, name, projectId }, (err, res) => {
      //if (!stories.length || err) return callback(err);
      if (!stories.length) return callback(err);
      const storyGroupId = _id;
      const title = activeConversationId;
      return Meteor.call(
        "stories.insert",
        stories.map((s) => ({ ...s, projectId, title, storyGroupId })),
        callback
      );
    });
  };

  function saveToStory(conversationId, story) {
    const s = story
      .replace(`## ${conversationId}\n`, "")
      .replace(/:.+(?=\n)/gm, "")
      .replace("* session_start\n", "");
    const stories = [{ story: s }];

    handleImportStoryGroups(stories);
    deleteConversation(conversationId);
  }

  function selectBotResponse(events) {
    setEvents(events);
  }

  function tooltipWrapper(trigger, tooltip) {
    return <Popup size="mini" inverted content={tooltip} trigger={trigger} />;
  }

  return (
    <div>
      <Grid centered>
        <Grid.Column width={3}>
          <Form onSubmit={handleSubmitName}>
            <Form.Field>
              {tooltipWrapper(
                <Input
                  icon="add"
                  placeholder="Conversation name..."
                  disabled={active}
                  value={conversationName}
                  onChange={(e, a) => {
                    const { value } = a;
                    setConversationName(value);
                  }}
                />,
                "New conversation"
              )}
            </Form.Field>
          </Form>
          {trackers.length > 0 && (
            <Menu pointing vertical fluid>
              {renderMenuItems()}
            </Menu>
          )}
          {pages > 1 ? (
            <Pagination
              totalPages={pages}
              onPageChange={(e, { activePage }) => pageChange(activePage)}
              activePage={page}
              boundaryRange={0}
              siblingRange={0}
              size="mini"
              firstItem="1"
              lastItem={`${pages}`}
            />
          ) : (
            <></>
          )}
        </Grid.Column>
        <Grid.Column width={5}>
          {trackers.length > 0 ? (
            <div>
              <ConversationLearner
                conversationId={activeConversationId}
                onSave={saveToStory}
                onClickBotResponse={selectBotResponse}
              />
              <Segment
                attached
                style={{ borderTop: 0, backgroundColor: "#f9f9f9" }}
              >
                <Form onSubmit={handleSubmitMessage}>
                  <Form.Field>
                    <Input
                      icon="send"
                      placeholder="Type message..."
                      disabled={active}
                      ref={inputEl}
                      value={userInput}
                      onChange={(e, a) => {
                        const { value } = a;
                        setUserInput(value);
                      }}
                    />
                  </Form.Field>
                </Form>
              </Segment>
            </div>
          ) : (
            <Message info>No conversation to load</Message>
          )}
        </Grid.Column>
        <Grid.Column width={7}>
          {confidences.length > 0 || events.length > 0 ? (
            <div>
              <ActionCandidates
                confidences={confidences}
                events={events}
                onSelect={runAction}
                replaceAction={replaceAndRunAction}
              />
            </div>
          ) : (
            <Message info>No bot responses to load</Message>
          )}
        </Grid.Column>
      </Grid>
    </div>
  );
}

InteractiveLearning.propTypes = {
  trackers: requiredIf(PropTypes.array, ({ loading }) => !loading),
  activeConversationId: PropTypes.string,
  page: PropTypes.number.isRequired,
  pages: PropTypes.number,
  projectId: PropTypes.string.isRequired,
  refetch: PropTypes.func.isRequired,
  router: PropTypes.object.isRequired,
};

InteractiveLearning.defaultProps = {
  trackers: [],
  activeConversationId: null,
  pages: 1,
};

const InteractiveLearningContainer = (props) => {
  const { router } = props;
  if (!router) {
    return <></>;
  }

  const projectId = router.params.project_id;
  let activeConversationId = router.params.selected_id;
  let page = parseInt(router.params.page, 10) || 1;
  if (!Number.isInteger(page) || page < 1) {
    page = 1;
  }

  const { loading, error, data, refetch } = useQuery(GET_CONVERSATIONS, {
    variables: { projectId, page, pageSize: 20 },
    pollInterval: 5000,
  });

  const componentProps = {
    page,
    projectId,
    refetch,
    router,
  };

  if (!loading && !error) {
    const conversations = data.conversationsPage.conversations.filter((c) =>
      c._id.includes(PREFIX_CONV)
    );
    const pages = data.conversationsPage.pages;

    // If for some reason the conversation is not in the current page, discard it.
    // if (!conversations.some((c) => c._id === activeConversationId))
    //   activeConversationId = null;

    if (!activeConversationId && conversations.length) {
      const url = updateInteractivePath({
        ...router.params,
        page: page || 1,
        selected_id: conversations[0]._id,
      });
      browserHistory.replace({ pathname: url });
    } else {
      Object.assign(componentProps, {
        trackers: conversations,
        activeConversationId,
        pages,
      });
    }
  } else {
    Object.assign(componentProps, {
      projectId,
      page,
    });
  }
  return (
    <>
      <PageMenu
        title="Teaching"
        icon="graduation"
        mode='core'>
          <Menu.Item>
            <LanguageDropdown />
          </Menu.Item>
      </PageMenu>
      <div>
        <Loading loading={loading}>
          <InteractiveLearning {...componentProps} />
        </Loading>
      </div>
    </>
  );
};

InteractiveLearningContainer.propTypes = {
  router: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  projectId: state.settings.get("projectId"),
});

const InteractiveLearningRouter = withRouter(InteractiveLearningContainer);

export default connect(mapStateToProps)(InteractiveLearningRouter);
