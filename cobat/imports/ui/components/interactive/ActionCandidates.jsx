import ReactTable from "react-table-v6";
import PropTypes from "prop-types";
import { Icon, Label, Menu, Segment } from "semantic-ui-react";
import React, { useState, useEffect } from "react";
import { useQuery } from "@apollo/react-hooks";
import { connect } from "react-redux";
import "react-s-alert/dist/s-alert-default.css";
import matchSorter from "match-sorter";
import TemplatesTableItem from "../templates/templates-list/TemplatesTableItem";
import { GET_BOT_RESPONSES } from "../templates/queries";
import { Loading } from "../utils/Utils";
import { changePageTemplatesTable } from "../../store/actions/actions";

class ActionCandidates extends React.Component {
  getColumns = (editBotResponse) => {
    const columns = [
      {
        id: "key",
        accessor: (t) => t.key,
        Header: "Actions",
        filterable: true,
        filterMethod: (filter, rows) =>
          matchSorter(rows, filter.value, { keys: ["key"] }),
        Cell: (props) => (
          <div data-cy="template-intent">
            <Label horizontal basic size="tiny">
              {props.value}
            </Label>
          </div>
        ),
        filterAll: true,
        width: 200,
      },
      {
        id: "responses",
        filterable: true,
        accessor: (t) => {
          const template = t.values[0];
          const { sequence = [] } = template || {};
          const filterableText = sequence
            .map((val) => (val || {}).content)
            .join("");
          return { sequence, filterableText };
        },
        filterMethod: (filter, rows) =>
          matchSorter(rows, filter.value, {
            keys: [`responses.filterableText`],
          }),
        Cell: ({ value: { sequence } }) => (
          <TemplatesTableItem sequence={sequence} />
        ),
        Header: "Responses",
        filterAll: true,
      },
      {
        id: "select",
        accessor: "key",
        className: "center",
        Cell: ({ value: key }) => {
          return (
            <Icon
              link
              name={editBotResponse ? "exchange" : "check"}
              color="grey"
              size="small"
              onClick={() => this.selectAction(key)}
            />
          );
        },
        width: 30,
      },
    ];

    if (!editBotResponse) {
      columns.unshift({
        id: "confidence",
        accessor: (t) => t.score,
        className: "center",
        Cell: (props) => (
          <span className="number">{`${
            Math.round((props.value * 100 + Number.EPSILON) * 100) / 100
          }%`}</span>
        ),
        width: 55,
      });
    }

    return columns;
  };

  selectAction = (key) => {
    const { onSelect, replaceAction, editBotResponse } = this.props;
    if (editBotResponse) replaceAction(key);
    else onSelect(key);
  };

  renderTable = () => {
    const { changePage, pageNumber, templates, editBotResponse } = this.props;
    return (
      <Segment attached style={{ borderTop: 0 }}>
        <ReactTable
          style={{ background: "#fff" }}
          data={templates}
          columns={this.getColumns(editBotResponse)}
          minRows={1}
          page={pageNumber}
          onPageChange={changePage}
          getTdProps={() => ({
            style: {
              display: "flex",
              verticalAlign: "top",
              flexDirection: "column",
            },
          })}
        />
      </Segment>
    );
  };

  render() {
    const { templates } = this.props;
    return (
      <div id="list-bot-actions">
        <Menu compact attached="top" style={{ borderBottom: 0 }}>
          <Menu.Item header>
            <span>Bot responses</span>
          </Menu.Item>
        </Menu>
        {templates.length > 0 && this.renderTable()}
      </div>
    );
  }
}

ActionCandidates.propTypes = {
  templates: PropTypes.array.isRequired,
  pageNumber: PropTypes.number.isRequired,
  changePage: PropTypes.func.isRequired,
  onSelect: PropTypes.func.isRequired,
  replaceAction: PropTypes.func.isRequired,
  editBotResponse: PropTypes.bool.isRequired,
};

const ActionCandidatesContainer = (props) => {
  const {
    confidences,
    projectId,
    pageNumber,
    changePage,
    onSelect,
    replaceAction,
    events,
  } = props;

  const [editBotResponse, setEditBotResponse] = useState(false);
  const [templates, setTemplates] = useState([]);

  const { loading, error, data, refetch } = useQuery(GET_BOT_RESPONSES, {
    variables: { projectId: projectId },
  });

  useEffect(() => {
    events.length > 0 ? setEditBotResponse(true) : setEditBotResponse(false);
  }, [events]);

  useEffect(() => {
    if (!loading && !error) {
      if (editBotResponse || !confidences.length > 0) {
        setTemplates(data.botResponses);
      } else {
        const arr1 = data.botResponses;
        const merged = [];

        for (let i = 0; i < arr1.length; i++) {
          merged.push({
            ...arr1[i],
            ...confidences.find((itmInner) => itmInner.action === arr1[i].key),
          });
        }
        action_listen = confidences.find(
          (itmInner) => itmInner.action === "action_listen"
        );
        merged.push({
          key: action_listen.action,
          score: action_listen.score,
          values: [],
        });
        setTemplates(merged.sort((a, b) => b.score - a.score));
      }
    }
  }, [data, confidences]);

  useEffect(() => {
    refetch();
  }, []);

  const componentProps = {
    templates,
    pageNumber,
    changePage,
    onSelect,
    replaceAction,
    editBotResponse,
  };

  return (
    <div>
      <Loading loading={loading}>
        <ActionCandidates {...componentProps} />
      </Loading>
    </div>
  );
};

const mapStateToProps = (state) => ({
  projectId: state.settings.get("projectId"),
  pageNumber: state.settings.get("templatesTablePage"),
});

const mapDispatchToProps = {
  changePage: changePageTemplatesTable,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ActionCandidatesContainer);
