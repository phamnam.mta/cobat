import { Meteor } from "meteor/meteor";
import ReactTable from "react-table-v6";
import Alert from "react-s-alert";
import { withTracker } from "meteor/react-meteor-data";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { cloneDeep, reduce, find } from "lodash";
import {
  Button,
  Confirm,
  Container,
  Header,
  Segment,
  Tab,
  Menu,
  Modal,
} from "semantic-ui-react";
import React from "react";
import {
  AutoForm,
  AutoField,
  ErrorsField,
  SubmitField,
} from "uniforms-semantic";
import { browserHistory } from "react-router";

import {
  UserEditSchema,
  UserCreateSchema,
} from "../../../api/user/user.schema";
import { can } from "../../../lib/scopes";
import { wrapMeteorCallback } from "../utils/Errors";
import ChangePassword from "./ChangePassword";
import { PageMenu } from "../utils/Utils";
import SelectField from "../form_fields/SelectField";
import IconButton from "../common/IconButton";

class User extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      confirmOpen: false,
      editProfileMode: {
        openModal: false,
        user: {},
      },
    };
    this.saveUser = this.saveUser.bind(this);
    this.formRef = null;
  }

  methodCallback = () =>
    wrapMeteorCallback((err) => {
      if (!err) {
        this.setState({
          editProfileMode: {
            openModal: false,
            user: {},
          },
        });
      } else {
        Alert.error(`Error: ${err.reason}`, {
          position: "top-right",
          timeout: "none",
        });
      }
    });

  saveUser = (user) => {
    const { projectId } = this.props;
    if (user._id) {
      Meteor.call("user.update", user, projectId, this.methodCallback());
    } else {
      const { sendEmail } = user;
      Meteor.call("user.Exists", user.email.trim(), (err, res) => {
        if (res) {
          Alert.error("Error: Email already exists", {
            position: "top-right",
            timeout: 1000,
          });
        } else {
          Meteor.call("user.create", user, !!sendEmail, projectId, (err) => {
            if (!err) {
              Alert.success("Created new user", {
                position: "top-right",
                timeout: 1000,
              });
            } else {
              Alert.error(`Error: ${err.reason}`, {
                position: "top-right",
                timeout: 1000,
              });
            }
          });
        }
        this.formRef.reset();
      });
    }
  };

  removeUser = (userId, role) => {
    const { projectId } = this.props;
    Meteor.call("user.remove", userId, role, projectId, this.methodCallback());
  };

  renderEditProfileContent = (user) => {
    const { confirmOpen } = this.state;
    const { userGroups, globalUsers } = this.props;
    const panes = [
      {
        menuItem: "General information",
        render: () => (
          <Segment>
            <AutoForm
              schema={UserEditSchema}
              onSubmit={(usr) => this.saveUser(usr)}
              model={user}
              modelTransform={(mode, model) => {
                const usr = cloneDeep(model);
                if (["validate", "submit"].includes(mode)) {
                  usr.email = model.emails[0].address.trim().toLowerCase();
                }
                return usr;
              }}
            >
              <AutoField name="emails.0.address" />
              <AutoField name="emails.0.verified" />
              <AutoField name="profile.firstName" />
              <AutoField name="profile.lastName" />
              {!globalUsers.includes(user._id) && (
                <SelectField name="role" options={userGroups} />
              )}
              <ErrorsField />
              <SubmitField />
            </AutoForm>
          </Segment>
        ),
      },
      {
        menuItem: "Password change",
        render: () => (
          <Segment>
            <ChangePassword userId={user._id} />
          </Segment>
        ),
      },
    ];

    if (can("global-admin") && !globalUsers.includes(user._id)) {
      panes.push({
        menuItem: "User deletion",
        render: () => (
          <Segment>
            <Header content="Delete user" />
            <br />
            <Button
              icon="trash"
              negative
              content="Delete user"
              onClick={() => this.setState({ confirmOpen: true })}
            />
            <Confirm
              open={confirmOpen}
              header={`Delete user ${user.emails[0].address}`}
              content="This cannot be undone!"
              onCancel={() => this.setState({ confirmOpen: false })}
              onConfirm={() => this.removeUser(user._id, user.role)}
            />
          </Segment>
        ),
      });
    }

    return (
      <Segment>
        <Tab menu={{ secondary: true, pointing: true }} panes={panes} />
      </Segment>
    );
  };

  getColumns = () => {
    const columns = [
      {
        id: "emails",
        accessor: (t) => t["emails"][0]["address"],
        Header: "Emails",
      },
      {
        id: "profile",
        accessor: (t) =>
          t["profile"]["firstName"] + " " + t["profile"]["lastName"],
        Header: "Name",
      },
      {
        accessor: "role",
        Header: "Role",
      },
      {
        accessor: "_id",
        Cell: (props) => {
          return (
            <div>
              <IconButton
                icon="edit"
                basic
                onClick={() => {
                  this.setState({
                    editProfileMode: {
                      openModal: true,
                      user: props.row._original,
                    },
                  });
                }}
              />
            </div>
          );
        },
        width: 50,
      },
    ];

    return columns;
  };

  getMenuPanes = () => {
    const headerStyle = {
      textAlign: "left",
      fontWeight: 800,
      paddingBottom: "10px",
    };
    const { userGroups, users } = this.props;
    return [
      {
        menuItem: <Menu.Item content="Add new user" key="Add new user" />,
        render: () => (
          <Tab.Pane>
            <AutoForm
              ref={(ref) => (this.formRef = ref)}
              schema={UserCreateSchema}
              onSubmit={this.saveUser}
            >
              <AutoField
                name="profile.firstName"
                placeholder="First name"
                label={null}
              />
              <AutoField
                name="profile.lastName"
                placeholder="Last name"
                label={null}
              />
              <AutoField name="email" placeholder="Email" label={null} />
              <AutoField
                name="password"
                placeholder="Choose a password"
                label={null}
                type="password"
              />
              <AutoField
                name="passwordVerify"
                placeholder="Confirm your password"
                label={null}
                type="password"
              />
              <SelectField
                name="role"
                options={userGroups}
                placeholder="User role"
                label={null}
              />
              <AutoField name="sendEmail" />
              <ErrorsField />
              <SubmitField label="Create user" className="primary" />
            </AutoForm>
          </Tab.Pane>
        ),
      },
      {
        menuItem: <Menu.Item content="List users" key="List users" />,
        render: () => (
          <Tab.Pane>
            {users.length > 0 ? (
              <ReactTable
                data={users}
                columns={this.getColumns()}
                minRows={1}
                defaultPageSize={5}
                style={{ overflow: "visible" }}
                getTbodyProps={() => ({
                  style: {
                    overflow: "visible",
                  },
                })}
                getTableProps={() => ({
                  style: {
                    overflow: "visible",
                  },
                })}
                getTdProps={() => ({
                  style: {
                    display: "flex",
                    flexDirection: "column",
                    justifyContent: "center",
                  },
                })}
                getTheadThProps={() => ({
                  style: {
                    borderRight: "none",
                    ...headerStyle,
                  },
                })}
              />
            ) : (
              <></>
            )}
          </Tab.Pane>
        ),
      },
    ];
  };

  render() {
    // noinspection JSAnnotator
    const { ready } = this.props;
    const { editProfileMode } = this.state;
    return (
      <>
        <Modal
          open
          centered
          content={
            editProfileMode.openModal
              ? this.renderEditProfileContent(editProfileMode.user)
              : ""
          }
          onClose={() =>
            this.setState({
              editProfileMode: {
                openModal: false,
                user: {},
              },
            })
          }
          open={editProfileMode.openModal}
        />
        <PageMenu icon="users" title="User management" />
        {ready && (
          <Tab
            menu={{ vertical: true }}
            grid={{ paneWidth: 12, tabWidth: 4 }}
            panes={this.getMenuPanes()}
          />
        )}
      </>
    );
  }
}

User.defaultProps = {
  users: [],
  userGroups: [],
  globalUsers: [],
};

User.propTypes = {
  users: PropTypes.array,
  userGroups: PropTypes.array,
  globalUsers: PropTypes.array,
  ready: PropTypes.bool.isRequired,
  projectId: PropTypes.string.isRequired,
};

const UserContainer = withTracker(() => {
  const userDataHandler = Meteor.subscribe("userData");
  const projectsHandler = Meteor.subscribe("projects.names");
  const ready = [userDataHandler, projectsHandler].every((h) => h.ready());

  let users = [];
  const tmpUsers = Meteor.users.find({}).fetch();
  const rolesAssigned = Meteor.roleAssignment.find({}).fetch();
  tmpUsers.forEach((u) => {
    const assigned = rolesAssigned.find((r) => r.user._id === u._id);
    users.push({
      ...u,
      role: !assigned ? "" : assigned.role._id,
    });
  });

  let userRoles = Meteor.roles.find({}).fetch();
  userRoles = userRoles.filter((g) => g.children.length > 0);
  let userGroups = [];
  userRoles.forEach((role) =>
    userGroups.push({
      text: role._id.charAt(0).toUpperCase() + role._id.slice(1),
      value: role._id,
    })
  );
  const globalUsers = Roles.getUsersInRole("admin", Roles.GLOBAL_GROUP)
    .fetch()
    .map((r) => r._id);

  return {
    ready,
    users,
    userGroups,
    globalUsers,
  };
})(User);

const mapStateToProps = (state) => ({
  projectId: state.settings.get("projectId"),
});

export default connect(mapStateToProps)(UserContainer);
