/* eslint-disable camelcase */
import { check, Match } from "meteor/check";
import queryString from "query-string";
import axiosRetry from "axios-retry";
import yaml from "js-yaml";
import axios from "axios";
import fs from "fs";
import { promisify } from "util";
import path from "path";
import uuidv4 from "uuid/v4";
import { isEmpty } from "lodash";

import {
  formatError,
  getProjectModelLocalFolder,
  getProjectModelFileName,
} from "../../lib/utils";
import { NLUModels } from "../nlu_model/nlu_model.collection";
import { getExamples } from '../graphql/examples/mongo/examples';
import { Instances } from "./instances.collection";
import { CorePolicies } from "../core_policies";
import { Evaluations } from "../nlu_evaluation";
import Activity from "../graphql/activity/activity.model";
import { getFragmentsAndDomain } from "../../lib/story.utils";
import { dropNullValuesFromObject } from '../../lib/client.safe.utils';
import { Projects } from '../project/project.collection';
import { checkIfCan } from '../../lib/scopes';

const replaceMongoReservedChars = (input) => {
  if (Array.isArray(input)) return input.map(replaceMongoReservedChars);
  if (typeof input === "object" && input !== null) {
    const corrected = input;
    Object.keys(input).forEach((key) => {
      const newKeyName = key.replace(/\./g, "_");
      corrected[newKeyName] = replaceMongoReservedChars(input[key]);
      if (newKeyName !== key) delete corrected[key];
    });
    return corrected;
  }
  return input;
};

export const createInstance = async (project) => {
  if (!Meteor.isServer) throw Meteor.Error(401, "Not Authorized");

  const { instance: host } = yaml.safeLoad(
    Assets.getText(
      process.env.MODE === "development"
        ? "defaults/private.dev.yaml"
        : "defaults/private.yaml"
    )
  );

  return Instances.insert({
    name: "Default Instance",
    host,
    projectId: project._id,
  });
};

export const getNluDataAndConfig = async ({ projectId, language, intents, status = -1, forCobat = false, isTestingData = false }) => {
  const model = await NLUModels.findOne(
      { projectId, language },
      { training_data: 1, config: 1 },
  );
  if (!model) {
      throw new Error(`Could not find ${language} model for project ${projectId}.`);
  }
  const copyAndFilter = ({
      _id, mode, min_score, ...obj
  }) => obj;
  const {
      training_data: { entity_synonyms, fuzzy_gazette, regex_features },
      config,
  } = model;
  const { examples = [] } = await getExamples({
      projectId,
      language,
      intents,
      status,
      isTestingData,
      pageSize: -1,
      sortKey: '_id',
      order: 'ASC',
  });
  const common_examples = examples.filter(e => !e?.metadata?.draft);
  const missingExamples = Math.abs(Math.min(0, common_examples.length - 2));
  for (let i = 0; (intents || []).length && i < missingExamples; i += 1) {
      common_examples.push({
          text: `${i}dummy${i}azerty${i}`,
          entities: [],
          metadata: { canonical: true, language },
          intent: `dumdum${i}`,
      });
  }

  return {
      rasa_nlu_data: {
        common_examples: !forCobat ? common_examples.map(
          ({
              text, intent, entities = [], metadata: { canonical, ...metadata } = {},
          }) => ({
              text,
              intent,
              entities: entities.map(({ _id: _, ...rest }) => dropNullValuesFromObject(rest)),
              metadata: {
                  ...metadata,
                  ...(canonical ? { canonical } : {}),
              },
          }),
      ) : common_examples,
          entity_synonyms: entity_synonyms.map(copyAndFilter),
          gazette: fuzzy_gazette.map(copyAndFilter),
          regex_features: regex_features.map(copyAndFilter),
      },
      config: yaml.dump({
          ...yaml.safeLoad(config),
          language,
      }),
  };
};

if (Meteor.isServer) {
  import {
    getAppLoggerForFile,
    getAppLoggerForMethod,
    addLoggingInterceptors,
  } from "../../../server/logger";
  // eslint-disable-next-line import/order
  import { performance } from "perf_hooks";

  const trainingAppLogger = getAppLoggerForFile(__filename);

  Meteor.methods({
    async 'rasa.parse'(instance, examples, options = {}) {
      check(instance, Object);
      check(examples, Array);
      check(options, Object);
      const { failSilently } = options;
      const appMethodLogger = getAppLoggerForMethod(
          trainingAppLogger,
          'rasa.parse',
          Meteor.userId(),
          { instance, examples },
      );
      appMethodLogger.debug('Parsing nlu');
      try {
          const client = axios.create({
              baseURL: instance.host,
              timeout: 100 * 1000,
          });
          addLoggingInterceptors(client, appMethodLogger);
          // axiosRetry(client, { retries: 3, retryDelay: axiosRetry.exponentialDelay });
          const requests = examples.map(({ text, lang }) => {
              const payload = Object.assign({}, { text, lang });
              const params = {};
              if (instance.token) params.token = instance.token;
              const qs = queryString.stringify(params);
              const url = `${instance.host}/model/parse?${qs}`;
              return client.post(url, payload);
          });

          const result = (await axios.all(requests))
              .filter(r => r.status === 200)
              .map(r => r.data)
              .map((r) => {
                  if (!r.text || r.text.startsWith('/')) {
                      return {
                          text: (r.text || '').replace(/^\//, ''),
                          intent: null,
                          intent_ranking: [],
                          entities: [],
                      };
                  }
                  return r;
              });
          if (result.length < 1 && !failSilently) {
              throw new Meteor.Error('Error when parsing NLU');
          }
          if (
              Array.from(new Set(result.map(r => r.language))).length > 1
              && !failSilently
          ) {
              throw new Meteor.Error(
                  'Tried to parse for more than one language at a time.',
              );
          }
          return examples.length < 2 ? result[0] : result;
      } catch (e) {
          if (failSilently) {
              const result = examples.map(({ text }) => ({
                  text: (text || '').replace(/^\//, ''),
                  intent: null,
                  intent_ranking: [],
                  entities: [],
              }));
              return examples.length < 2 ? result[0] : result;
          }
          throw formatError(e);
      }
  },

    async "rasa.messages"(projectId, conversationId, text) {
      check(projectId, String);
      check(conversationId, String);
      check(text, String);

      const appMethodLogger = getAppLoggerForMethod(
        trainingAppLogger,
        "rasa.messages",
        Meteor.userId(),
        {
          projectId,
          conversationId,
          text,
        }
      );
      try {
        const instance = Instances.findOne({ projectId });
        const client = axios.create({
          baseURL: instance.host,
          timeout: 100 * 1000,
          responseType: "json",
        });
        addLoggingInterceptors(client, appMethodLogger);
        const response = await client.post(
          `/conversations/${conversationId}/messages`,
          {
            text: text,
            sender: "user",
          }
        );

        return response.data;
      } catch (e) {
        let error = null;
        if (e.response) {
          // The request was made and the server responded with a status code
          // that falls out of the range of 2xx
          error = new Meteor.Error(e.response.status, e.response.data);
        } else if (e.request) {
          // The request was made but no response was received
          // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
          // http.ClientRequest in node.js
          error = new Meteor.Error("399", "timeout");
        } else {
          // Something happened in setting up the request that triggered an Error
          error = new Meteor.Error("500", e);
        }

        throw error;
      }
    },

    async "rasa.execute"(projectId, conversationId, name) {
      check(projectId, String);
      check(conversationId, String);
      check(name, String);

      const appMethodLogger = getAppLoggerForMethod(
        trainingAppLogger,
        "rasa.execute",
        Meteor.userId(),
        {
          projectId,
          conversationId,
          name,
        }
      );
      try {
        const instance = Instances.findOne({ projectId });
        const client = axios.create({
          baseURL: instance.host,
          timeout: 60 * 1000,
          responseType: "json",
        });
        addLoggingInterceptors(client, appMethodLogger);
        const response = await client.post(
          `/conversations/${conversationId}/execute`,
          {
            name: name,
          }
        );

        return response.data;
      } catch (e) {
        let error = null;
        if (e.response) {
          error = new Meteor.Error(e.response.status, e.response.data);
        } else if (e.request) {
          error = new Meteor.Error("399", "timeout");
        } else {
          error = new Meteor.Error("500", e);
        }

        throw error;
      }
    },

    async "rasa.tracker.events"(projectId, conversationId, events) {
      check(projectId, String);
      check(conversationId, String);
      check(events, Array);

      const appMethodLogger = getAppLoggerForMethod(
        trainingAppLogger,
        "rasa.tracker.events",
        Meteor.userId(),
        {
          projectId,
          conversationId,
          events,
        }
      );
      try {
        const instance = Instances.findOne({ projectId });
        const client = axios.create({
          baseURL: instance.host,
          timeout: 3 * 60 * 1000,
          responseType: "json",
        });
        addLoggingInterceptors(client, appMethodLogger);
        const response = await client.put(
          `/conversations/${conversationId}/tracker/events`,
          events
        );

        return response.data;
      } catch (e) {
        let error = null;
        if (e.response) {
          error = new Meteor.Error(e.response.status, e.response.data);
        } else if (e.request) {
          error = new Meteor.Error("399", "timeout");
        } else {
          error = new Meteor.Error("500", e);
        }

        throw error;
      }
    },

    async "rasa.story"(projectId, conversationId) {
      check(projectId, String);
      check(conversationId, String);

      const appMethodLogger = getAppLoggerForMethod(
        trainingAppLogger,
        "rasa.story",
        Meteor.userId(),
        {
          projectId,
          conversationId,
        }
      );
      try {
        const instance = Instances.findOne({ projectId });
        const client = axios.create({
          baseURL: instance.host,
          timeout: 60 * 1000,
          responseType: "json",
        });
        addLoggingInterceptors(client, appMethodLogger);
        const response = await client.get(
          `/conversations/${conversationId}/story`
        );

        return response.data;
      } catch (e) {
        let error = null;
        if (e.response) {
          error = new Meteor.Error(e.response.status, e.response.data);
        } else if (e.request) {
          error = new Meteor.Error("399", "timeout");
        } else {
          error = new Meteor.Error("500", e);
        }

        throw error;
      }
    },

    async 'rasa.data.convert'(json, projectId, language, outputFormat) {
      check(json, Object);
      check(language, String);
      check(projectId, String);
      check(outputFormat, String);
      
      const instance = await Instances.findOne({ projectId });
      const axiosClient = axios.create();
      const { data } = await axiosClient.post(`${instance.host}/data/convert/nlu`, {
        data: json,
        input_format: 'json',
        output_format: outputFormat,
        language,
      });
      return data.data || '';
    },

    async 'rasa.convertNluToJson'(rawText, extension, projectId, language) {
      check(rawText, String);
      check(extension, String);
      check(projectId, String);
      check(language, String);
      
      const instance = await Instances.findOne({ projectId });
      const axiosClient = axios.create();
      const { data } = await axiosClient.post(`${instance.host}/data/convert/nlu`, {
        data: extension === 'yaml' ? yaml.safeLoad(rawText) : rawText,
        input_format: extension === 'yaml' ? 'parsed_yaml' : extension,
        output_format: 'json',
        language,
      });
      return data.data || '';
    },

    async 'rasa.getTrainingPayload'(projectId, { language = '' } = {}, status = -1, forCobat = false, isTestingData = false) {
      check(projectId, String);
      check(language, String);
      check(status, Number);
      check(forCobat, Boolean);
      check(isTestingData, Boolean);
      const { policies: corePolicies, augmentationFactor } = CorePolicies.findOne(
        { projectId },
        { policies: 1, augmentationFactor: 1 },
    );
      const nlu = {};
      const config = {};

      const {
          stories = [], rules = [], domain, wasPartial,
      } = await getFragmentsAndDomain(
          projectId,
          language,
      );
      stories.sort((a, b) => a.story.localeCompare(b.story));
      rules.sort((a, b) => a.rule.localeCompare(b.rule));
      const selectedIntents = wasPartial
          ? yaml.safeLoad(domain).intents
          : undefined;
      let currentLang = language;
      if (!language) {
        const { defaultLanguage } = Projects.findOne({ _id: projectId }, { defaultLanguage: 1 });
        currentLang = defaultLanguage;
      }

      const {
        rasa_nlu_data,
        config: configForLang, 
      } = await getNluDataAndConfig({ projectId, language: currentLang, intents: selectedIntents, status, forCobat, isTestingData });

      nlu[currentLang] = { rasa_nlu_data };
      config[currentLang] = `${configForLang}\n\n${corePolicies}`;

      const payload = {
        domain: yaml.safeDump(domain, { skipInvalid: true, sortKeys: true }),
        stories,
        rules,
        nlu,
        config,
        fixed_model_name: getProjectModelFileName(projectId),
        agent: projectId,
      };
      return payload;
    },

    async "rasa.train"(projectId, workingLanguage = "", mode = "") {
      checkIfCan('nlu-model:w', projectId);
      check(projectId, String);
      check(workingLanguage, String);
      check(mode, String);
      const appMethodLogger = getAppLoggerForMethod(
        trainingAppLogger,
        "rasa.train",
        Meteor.userId(),
        { projectId, mode, workingLanguage }
      );

      appMethodLogger.debug(`Training project ${projectId}...`);
      const t0 = performance.now();
      const { trainingDataStatus } = NLUModels.findOne({ projectId, language: workingLanguage }, { fields: { trainingDataStatus: 1 } }) || {};
      const status = trainingDataStatus ? 1 : -1;
      try {
        const { stories = [], rules = [], ...payload } = await Meteor.call(
          'rasa.getTrainingPayload',
          projectId,
          { language: workingLanguage },
          status
        );
        payload.fragments = yaml.safeDump(
          { stories, rules },
          { skipInvalid: true },
      );

        const instance = await Instances.findOne({ projectId });
        const trainingClient = axios.create({
          baseURL: instance.host,
          timeout: 2 * 30 * 60 * 1000,
          responseType: "json",
        });
        addLoggingInterceptors(trainingClient, appMethodLogger);
        let url = "";
        if (mode === "core") {
          url = "/model/core";
        } else if (mode === "nlu") {
          url = "/model/nlu";
        } else {
          url = "/model/train";
        }
        const trainingResponse = await trainingClient.post(url, payload);
        if (trainingResponse.status === 200) {
          const t1 = performance.now();
          appMethodLogger.debug(
            `Training project ${projectId} - ${(t1 - t0).toFixed(2)} ms`
          );

          const trainedModelPath = trainingResponse.data.path.model_path;
          appMethodLogger.debug(`Saving model at ${trainedModelPath}`);
          const client = axios.create({
            baseURL: instance.host,
            timeout: 3 * 60 * 1000,
          });

          await client.put("/model", { model_file: trainedModelPath });

          Activity.update(
            { projectId, validated: true },
            { $set: { validated: false } },
            { multi: true },
          ).exec();

          // add a new model version to NLUModels
          const modelId = NLUModels.findOne(
            { projectId, language: workingLanguage },
            { fields: { _id: 1 } }
          )._id;

          const modelName = path.basename(trainedModelPath);

          const model = NLUModels.findOne(
            { _id: modelId },
            { fields: { versions: 1 } }
          );

          const versions = model.versions
            .filter((e) => {
              return e.name !== modelName;
            })
            .map((e) => {
              if (e.status === true) e.status = false;
              return e;
            });

          NLUModels.update(
            { _id: modelId },
            { $set: { versions: versions } }
          );
          NLUModels.update(
            { _id: modelId },
            {
              $push: {
                versions: {
                  _id: uuidv4(),
                  name: modelName,
                  path: trainingResponse.data.path || {},
                  status: true,
                  createdAt: new Date(),
                },
              },
            }
          );
        }
        Meteor.call("project.markTrainingStopped", projectId, "success");
      } catch (e) {
        const error = `${e.message || e.reason} ${(
          e.stack.split("\n")[2] || ""
        ).trim()}`;
        const t1 = performance.now();
        appMethodLogger.error(
          `Training project ${projectId} - ${(t1 - t0).toFixed(2)} ms`,
          { error }
        );
        Meteor.call(
          "project.markTrainingStopped",
          projectId,
          "failure",
          e.reason
        );
        throw formatError(e);
      }
    },

    async "rasa.deleteNlu"(ids, projectId, modelId) {
      checkIfCan('nlu-model:w', projectId);
      check(ids, Array);
      check(projectId, String);
      check(modelId, String);

      const appMethodLogger = getAppLoggerForMethod(
        trainingAppLogger,
        "rasa.deleteNlu",
        Meteor.userId(),
        { ids, projectId, modelId }
      );
      try {
        const model = NLUModels.findOne(
          { _id: modelId },
          { fields: { versions: 1 } }
        );
        const paths = model.versions
          .filter((e) => {
            return ids.includes(e._id);
          })
          .map((e) => e.path);

        const instance = Instances.findOne({ projectId });
        const client = axios.create({
          baseURL: instance.host,
          timeout: 100 * 1000,
        });
        addLoggingInterceptors(client, appMethodLogger);

        const response = await client.delete("/model", {
          data: {
            paths: paths,
          },
        });
        if (response.status === 204) {
          NLUModels.update(
            { _id: modelId },
            { $pull: { versions: { _id: { $in: ids } } } }
          );

          Evaluations.remove({ modelId: modelId, modelVersion: { $in: ids } });
        }

        return "ok";
      } catch (e) {
        console.log(e);
        let error = null;
        if (e.response) {
          error = new Meteor.Error(e.response.status, e.response.data);
        } else if (e.request) {
          error = new Meteor.Error("399", "timeout");
        } else {
          error = new Meteor.Error("500", e);
        }

        throw error;
      }
    },

    async "rasa.activeModel"(id, projectId, modelId) {
      checkIfCan('nlu-model:w', projectId);
      check(id, String);
      check(projectId, String);
      check(modelId, String);

      const appMethodLogger = getAppLoggerForMethod(
        trainingAppLogger,
        "rasa.activeModel",
        Meteor.userId(),
        { id, projectId, modelId }
      );
      try {
        const model = NLUModels.findOne(
          { _id: modelId },
          { fields: { versions: 1 } }
        );
        const modelPath = model.versions.find((x) => x._id === id).path
          .model_path;

        const instance = Instances.findOne({ projectId });
        const client = axios.create({
          baseURL: instance.host,
          timeout: 100 * 1000,
        });
        addLoggingInterceptors(client, appMethodLogger);

        const response = await client.put("/model", { model_file: modelPath });

        Activity.update(
          { projectId, validated: true },
          { $set: { validated: false } },
          { multi: true },
        ).exec();

        if (response.status === 204) {
          const versions = model.versions.map((e) => {
            if (e.status === true) e.status = false;
            if (e._id === id) e.status = true;
            return e;
          });

          NLUModels.update({ _id: modelId }, { $set: { versions: versions } });
        }

        return "ok";
      } catch (e) {
        let error = null;
        if (e.response) {
          error = new Meteor.Error(e.response.status, e.response.data);
        } else if (e.request) {
          error = new Meteor.Error("399", "timeout");
        } else {
          error = new Meteor.Error("500", e);
        }

        throw error;
      }
    },

    async "rasa.download"(id, projectId, modelId) {
      checkIfCan('nlu-model:w', projectId);
      check(id, String);
      check(projectId, String);
      check(modelId, String);

      const appMethodLogger = getAppLoggerForMethod(
        trainingAppLogger,
        "rasa.download",
        Meteor.userId(),
        { id, projectId, modelId }
      );
      try {
        const model = NLUModels.findOne(
          { _id: modelId },
          { fields: { versions: 1 } }
        );
        const modelPath = model.versions.find((x) => x._id === id).path;

        const instance = Instances.findOne({ projectId });
        const client = axios.create({
          baseURL: instance.host,
          timeout: 100 * 1000,
          responseType: "arraybuffer",
        });
        addLoggingInterceptors(client, appMethodLogger);

        const response = await client.get("/model/backup", {
          data: {
            path: modelPath,
          },
        });

        if (response.status === 200) {
          const {
            data,
            headers: { filename, "content-type": contentType },
          } = response;

          return { data, filename, contentType };
        }

        return {};
      } catch (e) {
        let error = null;
        if (e.response) {
          error = new Meteor.Error(e.response.status, e.response.data);
        } else if (e.request) {
          error = new Meteor.Error("399", "timeout");
        } else {
          error = new Meteor.Error("500", e);
        }

        throw error;
      }
    },

    async "rasa.evaluate.nlu"(modelId, modelVersion, projectId, language, testData, contentType, isTestingData = false) {
      checkIfCan('nlu-model:w', projectId);
      check(projectId, String);
      check(language, String);
      check(modelVersion, String);
      check(modelId, String);
      check(testData, Object);
      check(isTestingData, Boolean);
      check(contentType, String);

      const appMethodLogger = getAppLoggerForMethod(
        trainingAppLogger,
        "rasa.evaluate.nlu",
        Meteor.userId(),
        { modelId, modelVersion, projectId, language, testData }
      );
      try {
        this.unblock();
        const examples = isEmpty(testData) ? {
          rasa_nlu_data: (await getNluDataAndConfig({ projectId, language, isTestingData }))
              .rasa_nlu_data,
        } : testData;
        const params = { language };
        const instance = Instances.findOne({ projectId });
        if (instance.token) Object.assign(params, { token: instance.token });
        const qs = queryString.stringify(params);
        const client = axios.create({
          baseURL: instance.host,
          timeout: 60 * 60 * 1000,
          headers: {"content-type": 'application/x-yaml'}
        });
        addLoggingInterceptors(client, appMethodLogger);
        axiosRetry(client, {
          retries: 3,
          retryDelay: axiosRetry.exponentialDelay,
        });
        const url = `${instance.host}/model/test/intents?${qs}`;
        let results = Promise.await(client.post(url, examples));
        results = replaceMongoReservedChars({
          intent_evaluation: results.data.intent_evaluation || {},
          entity_evaluation:
            results.data.entity_evaluation.JointBertExtractor ||
            results.data.entity_evaluation.DIETClassifier ||
            {},
        });

        const evaluations = Evaluations.find(
          { modelId, modelVersion },
          { field: { _id: 1 } }
        ).fetch();
        if (evaluations.length > 0) {
          Evaluations.update(
            { _id: evaluations[0]._id },
            { $set: { results: results } }
          );
        } else {
          Evaluations.insert({ results: results, modelId, modelVersion });
        }
        return "ok";
      } catch (e) {
        throw formatError(e);
      }
    },
  });
}
