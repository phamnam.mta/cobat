import {
    getExamples,
    listIntentsAndEntities,
    assignExamples,
    insertExamples,
    updateExamples,
    deleteExamples,
    switchCanonical,
} from '../mongo/examples.js';
import { getIntentStatistics, getAnnotatorStatistics} from '../mongo/statistics';
import { can, checkIfCan } from '../../../../lib/scopes';

const { PubSub, withFilter } = require('apollo-server-express');

const pubsub = new PubSub();
const INTENTS_OR_ENTITIES_CHANGED = 'INTENTS_OR_ENTITIES_CHANGED';
const EXAMPLE_CHANGED = 'EXAMPLE_CHANGED';

export const publishIntentsOrEntitiesChanged = (projectId, language) => pubsub.publish(
    INTENTS_OR_ENTITIES_CHANGED,
    { projectId, language, intentsOrEntitiesChanged: { changed: true } },
);


export const subscriptionFilter = (payload, variables, context) => {
    if (
        can('nlu-data:r', payload.projectId, context.userId)
    ) {
        return payload.projectId === variables.projectId
        && payload.language === variables.language;
    }
    return false;
};

export default {
    Query: {
        async examples(_, { matchEntityName, countOnly, ...args }, context) {
            checkIfCan('nlu-data:r', args.projectId, context.user._id);
            return getExamples({ ...args, options: { matchEntityName, countOnly } });
        },
        async listIntentsAndEntities(_, args, context) {
            checkIfCan('nlu-data:r', args.projectId, context.user._id);
            return listIntentsAndEntities(args);
        },
        getIntentStatistics: async (_root, args, context) => {
            const { projectId, language } = args;
            checkIfCan('nlu-data:r', projectId, context.user._id);
            const stats = await getIntentStatistics({ projectId, language });
            return stats.map(({ intent, isTestingData }) => ({
                intent,
                example: (isTestingData[0] || {}).example || null,
                counts: isTestingData.map(({ example, ...rest }) => rest),
            }));
        },
        getAnnotatorStatistics: async (_root, args, context) => {
            const { projectId, language } = args;
            checkIfCan('nlu-data:r', projectId, context.user._id);
            const stats = await getAnnotatorStatistics({ projectId, language });
            return stats;
        },
    },
    Subscription: {
        intentsOrEntitiesChanged: {
            subscribe: withFilter(
                () => pubsub.asyncIterator([INTENTS_OR_ENTITIES_CHANGED]), subscriptionFilter
                ,
            ),
        },
        exampleChanged: {
            subscribe: withFilter(
                () => pubsub.asyncIterator([EXAMPLE_CHANGED]), subscriptionFilter
                ,
            ),
        },
    },
    Mutation: {
        async updateExamples(_, args, context) {
            checkIfCan('nlu-data:w', args.projectId, context.user._id);
            const response = await updateExamples({ ...args, userId: context.user._id });
            const { projectId, language } = args;
            publishIntentsOrEntitiesChanged(projectId, language);
            pubsub.publish(EXAMPLE_CHANGED, {
                projectId,
                language,
                exampleChanged: { changed: true },
            });
            return response;
        },
        async insertExamples(_, args, context) {
            checkIfCan('nlu-data:w', args.projectId, context.user._id);
            const response = await insertExamples({ ...args, userId: context.user._id });
            if ((response || []).length > 0) {
                const { projectId, language } = args;
                publishIntentsOrEntitiesChanged(projectId, language);
                pubsub.publish(EXAMPLE_CHANGED, {
                    projectId,
                    language,
                    exampleChanged: { changed: true },
                });
            }
            return response;
        },
        async deleteExamples(_, args, context) {
            checkIfCan('nlu-data:w', args.projectId, context.user._id);
            const response = await deleteExamples(args);
            const { projectId, language } = args;
            publishIntentsOrEntitiesChanged(projectId, language);
            pubsub.publish(EXAMPLE_CHANGED, {
                projectId,
                language,
                exampleChanged: { changed: true },
            });
            return response;
        },
        async switchCanonical(_, args, context) {
            checkIfCan('nlu-data:w', args.projectId, context.user._id);
            const response = await switchCanonical({ ...args, userId: context.user._id });
            const { projectId, language } = args;
            publishIntentsOrEntitiesChanged(projectId, language);
            pubsub.publish(EXAMPLE_CHANGED, {
                projectId,
                language,
                exampleChanged: { changed: true },
            });
            return response;
        },
        async assignExamples(_, args, context) {
            checkIfCan('nlu-data:w', args.projectId, context.user._id);
            const response = await assignExamples({ ...args, user: context.user });
            return response;
        },
    },

    ExamplePage: {
        examples: (parent, _, __) => parent.examples,
        pageInfo: (parent, _, __) => parent.pageInfo,
    },
    IntentsAndEntitiesList: {
        intents: ({ intents }) => intents,
        entities: ({ entities }) => entities,
    },
    Example: {
        projectId: (parent, _, __) => parent.projectId,
        _id: (parent, _, __) => parent._id,
        text: (parent, _, __) => parent.text,
        intent: (parent, _, __) => parent.intent,
        entities: (parent, _, __) => parent.entities,
        status: (parent, _, __) => parent.status,
        isTestingData: (parent, _, __) => parent.isTestingData,
        metadata: (parent, _, __) => parent.metadata,
        updatedAt: (parent, _, __) => parent.updatedAt,
        updatedBy: (parent, _, __) => parent.updatedBy,
    },
    NluStatistics: {
        intent: ({ intent }) => intent,
        example: ({ example }) => example,
        counts: ({ counts }) => counts,
    },
    AnnotatorStatistics: {
        annotator: ({ annotator }) => annotator,
        status: ({ status }) => status,
    },
    AnnotatorStatisticsByStatus: {
        value: ({ value }) => value,
        count: ({ count }) => count,
    },
    PageInfo: {
        endCursor: (parent, _, __) => parent.endCursor,
        hasNextPage: (parent, _, __) => parent.hasNextPage,
        doing: (parent, _, __) => parent.doing,
    },
};
