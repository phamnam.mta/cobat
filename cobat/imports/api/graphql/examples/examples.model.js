import mongoose from 'mongoose';

const { Schema } = mongoose;
const model = new Schema({
    _id: String,
    createdAt: Date,
    updatedAt: Date,
    updatedBy: String,
    projectId: String,
    text: String,
    intent: String,
    isTestingData:{
        type: Boolean,
        default: false
    },
    entities: [{
        start: Number,
        end: Number,
        value: String,
        entity: String,
        _id: false,
    }],
    status: {
        type: Number,
        default: 0
    },
    annotator: {
        _id: {
            type: String,
            default: null
        },
        email: String,
        name: String,
    },
    metadata: { type: Object, required: false },
}, { strict: false });

model.index({ text: 1, projectId: 1, 'metadata.language': 1, isTestingData:1 }, { unique: true });

module.exports = mongoose.model('Examples', model, 'examples');
