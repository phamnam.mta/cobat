import Examples from '../examples.model.js';

export const getIntentStatistics = async ({ projectId, language }) => Examples.aggregate([
    {
        $match: {
            projectId: projectId, 'metadata.draft': { $ne: true }, 'metadata.language': language,
        },
    },
    {
        $sort: { 'metadata.canonical': -1 },
    },
    {
        $group: {
            _id: {
                intent: '$intent',
                isTestingData: '$isTestingData',
            },
            count: { $sum: 1 },
            example: {
                $first: {
                    text: '$text',
                },
            },
        },
    },
    {
        $group: {
            _id: '$_id.intent',
            isTestingData: {
                $push: {
                    type: '$_id.isTestingData',
                    count: '$count',
                    example: '$example',
                },
            },
        },
    },
    {
        $project: {
            intent: '$_id',
            _id: false,
            isTestingData: 1
        },
    },
    { $sort: { isTestingData: -1 } },
]).allowDiskUse(true);

export const getAnnotatorStatistics = async ({ projectId, language }) => Examples.aggregate([
    {
        $match: {
            projectId, 'metadata.draft': { $ne: true }, 'metadata.language': language,
        },
    },
    {
        $sort: { 'metadata.canonical': -1 },
    },
    {
        $group: {
            _id: {
                annotator: '$annotator',
                status: '$status',
            },
            count: { $sum: 1 },
        },
    },
    {
        $group: {
            _id: '$_id.annotator',
            status: {
                $push: {
                    value: '$_id.status',
                    count: '$count',
                },
            },
        },
    },
    {
        $project: {
            annotator: '$_id',
            _id: false,
            status: 1,
        },
    },
    { $sort: { annotator: 1 } },
]).allowDiskUse(true);