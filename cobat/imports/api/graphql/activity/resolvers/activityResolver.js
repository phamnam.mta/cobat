import {
    getActivity,
    upsertActivity,
    deleteActivity,
} from '../mongo/activity';
import { can, checkIfCan } from '../../../../lib/scopes';

const { PubSub, withFilter } = require('apollo-server-express');
const pubsub = new PubSub();
const ACTIVITY_CHANGED = 'ACTIVITY_CHANGED';

export const subscriptionFilter = (payload, variables, context) => {
    if (
        can('nlu-data:r', payload.projectId, context.userId)
    ) {
        return payload.projectId === variables.projectId
        && payload.language === variables.language;
    }
    return false;
};

export default {
    Query: {
        getActivity: async (_root, args) => {
            const { cursor, pageSize } = args;
            const data = await getActivity(args);
            const cursorIndex = !cursor
                ? 0
                : data.findIndex(activity => activity._id === cursor) + 1;
            const activity = pageSize === 0
                ? data
                : data.slice(cursorIndex, cursorIndex + pageSize);
            return {
                activity,
                pageInfo: {
                    endCursor: activity.length ? activity[activity.length - 1]._id : '',
                    hasNextPage: cursorIndex + pageSize < data.length,
                    totalLength: data.length,
                },
            };
        },
    },

    Subscription: {
        activityChanged: {
            subscribe: withFilter(
                () => pubsub.asyncIterator([ACTIVITY_CHANGED]), subscriptionFilter
                ,
            ),
        },
    },

    Mutation: {
        upsertActivity: async (_root, args, context) => { 
            checkIfCan('nlu-data:w', args.projectId, context.user._id);
            const response = await upsertActivity(args);
            const { projectId, language } = args;
            pubsub.publish(ACTIVITY_CHANGED, {
                projectId,
                language,
                activityChanged: { changed: true },
            });
            return response;
        },
        deleteActivity: async (_root, args, context) => {
            checkIfCan('nlu-data:w', args.projectId, context.user._id);
            const response = await deleteActivity(args);
            const { projectId, language } = args;
            pubsub.publish(ACTIVITY_CHANGED, {
                projectId,
                language,
                activityChanged: { changed: true },
            });
            return response;
        },
    },

    Activity: {
        _id: ({ _id }) => _id,
        projectId: ({ projectId }) => projectId,
        language: ({ language }) => language,
        text: ({ text }) => text,
        intent: ({ intent }) => intent,
        entities: ({ entities }) => entities,
        confidence: ({ confidence }) => confidence,
        validated: ({ validated }) => validated,
        createdAt: ({ createdAt }) => createdAt,
        updatedAt: ({ updatedAt }) => updatedAt,
    },

    Entity: {
        start: ({ start }) => start,
        end: ({ end }) => end,
        value: ({ value }) => value,
        entity: ({ entity }) => entity,
        confidence: ({ confidence }) => confidence,
        extractor: ({ extractor }) => extractor,
        processors: ({ processors }) => processors,
    },
    
};
