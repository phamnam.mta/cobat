import { Meteor } from 'meteor/meteor';
import { check } from 'meteor/check';
import { Accounts } from 'meteor/accounts-base';
import util from 'util';

import queryString from 'query-string';
import axios from 'axios';
import { GlobalSettings } from '../globalSettings/globalSettings.collection';
import { checkIfCan, can, setScopes } from '../../lib/scopes';

export const passwordComplexityRegex = /^(?:(?=.*[a-z])(?:(?=.*[A-Z])(?=.*[\d\W])|(?=.*\W)(?=.*\d))|(?=.*\W)(?=.*[A-Z])(?=.*\d)).{9,}$/;

if (Meteor.isServer) {
    import { getAppLoggerForMethod, getAppLoggerForFile, addLoggingInterceptors } from '../../../server/logger';

    const userAppLogger = getAppLoggerForFile(__filename);

    Meteor.publish('userData', function() {
        if (can('global-admin')) {
            return [
                Meteor.users.find({ username: { $ne: 'EXTERNAL_CONSUMER' } }, {fields: {emails: 1, profile: 1}}),
                Meteor.roles.find({}),
                Meteor.roleAssignment.find({})
            ];
        }
        else {
            return [
                Meteor.users.find({ username: { $ne: 'EXTERNAL_CONSUMER' } }, {fields: {emails: 1, profile: 1}}),
                Meteor.roleAssignment.find({ 'user._id': this.userId })
            ];
        }
    });

    Meteor.methods({
        'user.create'(user, sendInviteEmail, projectId) {
            check(user, Object);
            check(sendInviteEmail, Boolean);
            check(projectId, String);
            checkIfCan('global-admin');
            try {
                const userId = Accounts.createUser({
                    email: user.email.trim(),
                    password: user.password,
                    profile: {
                        firstName: user.profile.firstName,
                        lastName: user.profile.lastName,
                    },
                });
                // this.unblock();
                // if (sendInviteEmail) Accounts.sendEnrollmentEmail(userId);
                Roles.addUsersToRoles(userId, user.role, projectId);
                return userId;
            } catch (e) {
                console.log(e);
                throw e;
            }
        },

        'user.update'(user, projectId) {
            checkIfCan('global-admin');
            check(user, Object);
            check(projectId, String);
            try {
                Meteor.users.update(
                    { _id: user._id },
                    {
                        $set: {
                            profile: user.profile,
                            'emails.0.address': user.emails[0].address,
                            'emails.0.verified': true,
                        },
                    },
                );
                const ids = Roles.getUsersInRole('admin', Roles.GLOBAL_GROUP).fetch().map(r => r._id);
                if(!ids.includes(user._id)) Roles.setUserRoles(user._id, user.role, projectId);
            } catch (e) {
                throw e;
            }
        },

        'user.remove'(userId, role, projectId) {
            checkIfCan('global-admin');
            check(userId, String);
            check(role, String);
            check(projectId, String);
            try {
                const ids = Roles.getUsersInRole('admin', Roles.GLOBAL_GROUP).fetch().map(r => r._id);
                if(!ids.includes(userId)) {
                    Roles.removeUsersFromRoles(userId, role, projectId)
                    return Meteor.users.remove({ _id: userId });
                }
                else {
                    throw new Meteor.Error('Error', "You can\'t delete the default user");
                }
            } catch (e) {
                throw e;
            }
        },

        'user.changePassword'(userId, newPassword) {
            checkIfCan('global-admin');
            check(userId, String);
            check(newPassword, String);

            try {
                return Promise.await(Accounts.setPassword(userId, newPassword));
            } catch (e) {
                throw e;
            }
        },

        'users.checkEmpty'() {
            try {
                // don't count EXTERNAL_CONSUMER for Welcome screen check
                return Meteor.users.find({ username: { $ne: 'EXTERNAL_CONSUMER' } }).count() === 0;
            } catch (e) {
                throw e;
            }
        },

        "user.Exists": function(email){
            check(email, String);
            return !!Accounts.findUserByEmail(email);   // casts to a boolean, "true" if exists, "false" otherwise
         },

        'user.verifyReCaptcha'(response) {
            const appMethodLogger = getAppLoggerForMethod(
                userAppLogger,
                'user.verifyReCaptcha',
                Meteor.userId(),
                { response },
            );

            check(response, String);
            const {
                settings: { private: { reCatpchaSecretServerKey: secret = null } = {} } = {},
            } = GlobalSettings.findOne(
                {},
                { fields: { 'settings.private.reCatpchaSecretServerKey': 1 } },
            );
            const qs = queryString.stringify({ response, secret });
            try {
                this.unblock();
                const url = `https://www.google.com/recaptcha/api/siteverify?${qs}`;
                const reCaptchaAxios = axios.create();
                addLoggingInterceptors(reCaptchaAxios, appMethodLogger);
                const result = Promise.await(reCaptchaAxios.post(url));
                if (result.data.success) return 'OK';
                throw new Meteor.Error(
                    '403',
                    'Something went wrong when verifying the reCaptcha. Please try again.',
                );
            } catch (e) {
                throw e;
            }
        },
    });
}
