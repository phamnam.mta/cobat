import SimpleSchema from 'simpl-schema';
import { passwordComplexityRegex } from './user.methods';

export const UserEditSchema = new SimpleSchema({
    _id: { type: String },
    emails: Array,
    'emails.$': Object,
    'emails.$.address': { type: String, regEx: SimpleSchema.RegEx.Email },
    'emails.$.verified': { type: Boolean, autoValue: true },
    profile: Object,
    'profile.firstName': { type: String },
    'profile.lastName': { type: String },
    role: String,
}, { tracker: Tracker });

export const UserCreateSchema = new SimpleSchema({
    profile: Object,
    'profile.firstName': { type: String, min: 1 },
    'profile.lastName': { type: String, min: 1 },
    email: { type: String, regEx: SimpleSchema.RegEx.EmailWithTLD },
    password: {
        type: String,
        custom() {
            return !this.value.match(passwordComplexityRegex) ? 'passwordTooSimple' : null;
        },
    },
    passwordVerify: {
        type: String,
        custom() {
            return this.value !== this.field('password').value ? 'passwordMismatch' : null;
        },
    },
    role: String,
    sendEmail: { type: Boolean, defaultValue: true },
}, { tracker: Tracker });
